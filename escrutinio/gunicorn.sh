#!/bin/bash
cd /opt/escrutinio
source venv/bin/activate
cd /opt/escrutinio/escrutinio
gunicorn escrutinio.wsgi -t 600 -b 127.0.0.1:8000 --workers=13 --user=servidor --group=servidor --log-file=/opt/escrutinio/gunicorn.log 2>>/opt/escrutinio/gunicorn.log
