#Listado de mesas cargadas > Consistencias
#Proyeccion USAR mesas testigos y proyectando por municipio
# POST ELECCIONES:
# RE ESCRIBIR SISTEMA DE CARGOS - VOTOS PARA QUE PUEDAN IR A NACION - PROVINCIA - MUNCIPIO

#Modulos Python
import csv
import copy
import logging
from operator import itemgetter
#Modulos Django
from django.apps import apps
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponse, HttpResponseRedirect
from django.db.models import Sum, Count
from django.core.cache import cache
#Agregamos modulos personales
from .decoradores import superuser_required
from .generic_classes import Charts, Items
from .models import Eleccion, Columna, Total, Cargo, CantCargos
from .models import Provincia, Municipio, Circuito
from .models import Agrupacion, Lista
from .models import Mesa, Acta, Voto, Conteo
from .models import Voto_Resumen, Conteo_Resumen
from .models import Responsable, Padron, Votante
from .modelforms import ResponsableForm, ReplicarActasForm, VotanteForm
from .functions import generar_columnas, generar_cargos, generar_listas, generar_totales
from .functions import replicar_listas
from .functions import obtener_cvotantes, proyectar, proyectar_bruto
from .functions import actualizar_resultados
from .functiones_testigo import actualizar_testigos
from .uploads import cargar_geografico, cargar_mesas, cargar_padron, cargar_votos
from .forms import ActaForm, UploadCsv, UploadPadron, UploadVotosForm, UploadMesasTestigo
from .forms import ReplicarEleccionForm, DownloadOficialesForm
from .forms import CruceDiferencial, ControlEstadistico, ControlNoPositivos, ControlxLimites
from .tasks import task_progress, crear_progress_link, download_mesas

def home(request):
    elecciones = Eleccion.objects.all()
    if elecciones:
        eleccion = elecciones.filter(activa=True).first()
    else:
        elecciones = None
    if not eleccion:
        eleccion = None
    return render(request, 'home.html', {'elecciones': elecciones, 'eleccion': eleccion, })

def contacto(request):
    return render(request, 'extras/contacto.html', {})

@staff_member_required
def controles(request):
    return render(request, 'controles/listado.html', { })

def donde_voto(request):
    if request.method == 'POST':
        form = VotanteForm(request.POST)
        if form.is_valid():
            try:
                votante = Votante.objects.get(padron__actual=True, num_doc=form.cleaned_data.get('num_doc'))
                return render(request, 'extras/votante.html', {'votante': votante,})
            except:
                return render(request, 'extras/votante.html', {'votante': None, })
    else:
        form = VotanteForm()
    return render(request, 'utils/generic_form.html', {'form': form, 'titulo_header': 'Padrón Electoral de Jujuy', })

#CARGA
@staff_member_required
def select_acta(request, eleccion_id):
    eleccion = Eleccion.objects.get(pk=eleccion_id)
    if request.method == 'POST':#Si nos envian informacion
        try:
            mesa = Mesa.objects.get(escuela__circuito__municipio__seccion__provincia__eleccion=eleccion, numero=request.POST['mesa_numero'])
        except:
            mesa = None
        if mesa is None:
            return HttpResponseRedirect("/contacto")
        else:
            return HttpResponseRedirect("/cargar/"+str(eleccion.id)+"/"+str(mesa.numero))
    return render(request, 'cargar/select_acta.html', {'eleccion': eleccion, })

@staff_member_required
def cargar_acta(request, eleccion_id, mesa_num):
    carga = False
    eleccion = Eleccion.objects.get(pk=eleccion_id)
    mesa = Mesa.objects.select_related('escuela', 'escuela__circuito', 'escuela__circuito__municipio', ).get(escuela__circuito__municipio__seccion__provincia__eleccion=eleccion, numero=mesa_num)
    acta = Acta.objects.select_related('mesa', ).get(mesa=mesa)
    header = generar_columnas(eleccion_id, 1, 3, mesa.extranjeros)#Generamosel header para (eleccion,tipo:carga,cargos:municipal)> segun tipos en models
    dict_cargos = generar_cargos(eleccion, 3, mesa.extranjeros)#Cargamos un dict-cache para evitar hits a la db
    dict_listas = generar_listas(mesa)#Cargamos un dict-cache para evitar hits a la db
    dict_totales = generar_totales(eleccion)#Cargamos un dict-cache para evitar hits a la db
    votos = Voto.objects.select_related('mesa', 'lista', 'cargo', ).filter(mesa=mesa)
    conteos = Conteo.objects.select_related('mesa', 'total', 'cargo', ).filter(mesa=mesa)
    if request.method == 'POST':#Si nos envian informacion
        votos.delete()#Borramos los votos viejos
        conteos.delete()#Borramos los conteos viejos
        votos, conteos = [], []#Creamos listas para nuevos votos y conteos
        for item in request.POST:#Por cada elemento recibido del form
            if item[:2] == "v:" and request.POST[item] and request.POST[item] != '0':#Si es un voto y no esta vacio
                lv = item[2:].split('-')#Lo parseamos
                votos.append(Voto(mesa=mesa, lista=dict_listas[int(lv[1])], cargo=dict_cargos[int(lv[2])], cantidad=int(request.POST[item])))
            if item[:2] == "t:" and request.POST[item] and request.POST[item] != '0':#Si es un conteo y no esta vacio
                lc = item[2:].split('-')#Lo parseamos
                conteos.append(Conteo(mesa=mesa, total=dict_totales[int(lc[1])], cargo=dict_cargos[int(lc[2])], cantidad=int(request.POST[item])))
        if votos or conteos:
            carga = True
            Voto.objects.bulk_create(votos)#Si se ingresaron votos, los mandamos masivamente
            Conteo.objects.bulk_create(conteos)#Si se ingresaron conteos, los mandamos masivamente
    else:
        carga = False
        votos, conteos = list(votos), list(conteos)
    return render(request, 'cargar/cargar_acta.html', {'eleccion': eleccion, 'mesa': mesa, 'header': header,
                                                       'totales':dict_totales.values(), 'listas': dict_listas.values(), 
                                                       'votos': votos, 'conteos': conteos, 'acta': acta, 'carga':carga,})

@staff_member_required
def cargar_scan(request, eleccion_id, mesa_num):
    eleccion = Eleccion.objects.get(pk=eleccion_id)
    mesa = Mesa.objects.select_related('acta', 'escuela', 'escuela__circuito', 'escuela__circuito__municipio', ).get(escuela__circuito__municipio__seccion__provincia__eleccion=eleccion, numero=mesa_num)
    if request.method == "POST":
        carga = True
        form = ActaForm(request.POST, request.FILES, instance=mesa.acta)
        if form.is_valid():
            form.save()
    else:
        carga = False
        form = ActaForm(instance=mesa.acta)
    return render(request, 'cargar/cargar_scan.html', {'eleccion': eleccion, 'mesa': mesa, 'carga': carga, 'form': form,})

#RESULTADOS
def resultados(request, eleccion_id, municipio_id=None, circuito_id=None):
    eleccion = Eleccion.objects.get(pk=eleccion_id)
    municipios = Municipio.objects.filter(seccion__provincia__eleccion=eleccion).order_by('alpha_id')
    dict_totales = generar_totales(eleccion)#Cargamos un dict-cache para evitar hits a la db
    chart = Charts()
    chart.cache_name += 'result-'
    chart.table= []#PREGUNTARLE A FISA XQ si el objeto esta recien creado trae datos viejos (?)!!!!!
    proyectado = 0

    if request.method == 'POST':
        if "id_municipio" in request.POST:
            return HttpResponseRedirect(request.path[:12]+str(eleccion.id)+"/"+str(request.POST["id_municipio"]))
        elif "id_circuito" in request.POST:
            return HttpResponseRedirect(request.path[:12]+str(eleccion.id)+"/"+str(municipio_id)+"/"+str(request.POST["id_circuito"]))

    actualizar_resultados(eleccion)
    
    if request.path[:12] == '/proyectado/':
        proyectado = 1
        chart.titulo+= ' Proyectado'
    elif request.path[:17] == '/proyectadobruto/':
        proyectado = 2
        chart.titulo+= ' Proyectado BRUTO'
    
    if circuito_id:
        municipio= Municipio.objects.get(pk=municipio_id)
        circuito = Circuito.objects.get(pk=circuito_id)
        circuitos = Circuito.objects.filter(municipio=municipio)
        chart.titulo+= ' (Circuito)'
        chart.cargo = Cargo.objects.filter(eleccion=eleccion, poder=1, tipo=3).first()
        chart.titulo+= ' para ' + chart.cargo.nombre
        header= generar_columnas(eleccion_id, 2, 3)#Generamos el header (eleccion, resultado, municipal)> segun tipos en models
        listas = Lista.objects.select_related('agrupacion').prefetch_related('cargos').filter(municipio=municipio).order_by('agrupacion__alpha_id', 'orden').distinct()
        votos = [v for v in Voto.objects.select_related('lista', 'lista__agrupacion', 'cargo', 'mesa__escuela__circuito__municipio').filter(mesa__escuela__circuito=circuito)]
        conteos = [c for c in Conteo.objects.select_related('total', 'cargo', 'mesa__escuela__circuito__municipio').filter(mesa__escuela__circuito=circuito)]
        if proyectado == 1:
            votos, conteos = proyectar(eleccion, votos, municipio, conteos)
        elif proyectado == 2:
            votos, conteos = proyectar_bruto(eleccion, votos, municipio, conteos)
        #charts:
        for lista in listas:
            chart.table.append([lista.nombre, sum(v.cantidad for v in votos if v.lista == lista and v.cargo==chart.cargo)])
        chart.table.sort(key=itemgetter(1), reverse=True)
        chart.table = [['Partido', 'Votos']] + chart.table[0:5] + [['otros', sum(cant[1] for cant in chart.table[6:])]]
        chart.cache_name += str(eleccion.id)+'-'+str(chart.cargo.id)+'-'+str(municipio.id)
        cache.set(chart.cache_name, chart)
        return render(request, 'resultados/resultados.html', {'eleccion': eleccion, 'municipio': municipio, 'circuito': circuito,
                                        'header': header, 'totales':dict_totales.values(), 'listas': listas, 
                                        'votos': votos, 'conteos': conteos,
                                        'municipios': municipios, 'circuitos': circuitos, 'chart': chart,
                                        'refresh': 120,})   
    elif municipio_id:
        municipio= Municipio.objects.get(pk=municipio_id)
        circuitos = Circuito.objects.filter(municipio=municipio)
        chart.titulo+= ' (Municipal)'
        chart.cargo = Cargo.objects.filter(eleccion=eleccion, poder=1, tipo=3).first()
        chart.titulo+= ' para ' + chart.cargo.nombre
        header= generar_columnas(eleccion_id, 2, 3)#Generamos el header (eleccion, resultado, municipal)> segun tipos en models
        listas = Lista.objects.select_related('agrupacion').prefetch_related('cargos').filter(municipio=municipio).order_by('agrupacion__alpha_id', 'orden').distinct()
        votos = [v for v in Voto_Resumen.objects.select_related('lista', 'lista__agrupacion', 'cargo', 'municipio').filter(municipio=municipio)]
        conteos = [c for c in Conteo_Resumen.objects.select_related('total', 'cargo', 'municipio').filter(municipio=municipio)]
        if proyectado == 1:
            votos, conteos = proyectar(eleccion, votos, municipio, conteos)
        elif proyectado == 2:
            votos, conteos = proyectar_bruto(eleccion, votos, municipio, conteos)
        #charts:
        for lista in listas:
            chart.table.append([lista.nombre, sum(v.cantidad for v in votos if v.lista == lista and v.cargo==chart.cargo)])
        chart.table.sort(key=itemgetter(1), reverse=True)
        chart.table = [['Partido', 'Votos']] + chart.table[0:5] + [['otros', sum(cant[1] for cant in chart.table[6:])]]
        chart.cache_name += str(eleccion.id)+'-'+str(chart.cargo.id)+'-'+str(municipio.id)
        cache.set(chart.cache_name, chart)
        return render(request, 'resultados/resultados.html', {'eleccion': eleccion, 'municipio': municipio, 'header': header, 
                                        'totales':dict_totales.values(), 'listas': listas, 
                                        'votos': votos, 'conteos': conteos,
                                        'municipios': municipios, 'circuitos': circuitos, 'chart': chart,
                                        'refresh': 120,})
    else:#Si es resultados generales
        chart.titulo+= ' (Provincial)'
        chart.cargo = Cargo.objects.filter(eleccion=eleccion, poder=1, tipo=2).first()
        chart.titulo+= ' para ' + chart.cargo.nombre
        header = generar_columnas(eleccion_id, 2, 2)#Generamos el header (eleccion, resultado, provincial)> segun tipos en models
        cargos = Cargo.objects.filter(eleccion=eleccion, tipo=2)
        agrupaciones = Agrupacion.objects.prefetch_related('listas', 'listas__cargos').filter(listas__cargos__in=cargos).order_by('alpha_id').distinct()
        votos = [v for v in  Voto_Resumen.objects.select_related('lista', 'cargo', 'lista__agrupacion', 'municipio').filter(cargo__in=cargos)]
        conteos = [c for c in Conteo_Resumen.objects.select_related('total', 'cargo', 'municipio').filter(cargo__in=cargos)]
        if proyectado == 1:
            votos, conteos = proyectar(eleccion, votos, None, conteos)
        elif proyectado == 2:
            votos, conteos = proyectar_bruto(eleccion, votos, None, conteos)
        #Charts
        for agrupacion in agrupaciones:
            chart.table.append([agrupacion.nombre, sum(v.cantidad for v in votos if v.lista.agrupacion == agrupacion and v.cargo==chart.cargo)])
        chart.table.sort(key=itemgetter(1), reverse=True)
        chart.table = [['Partido', 'Votos']] + chart.table[0:5] + [['otros', sum(cant[1] for cant in chart.table[6:])]]
        chart.cache_name += str(eleccion.id)+'-'+str(chart.cargo.id)
        cache.set(chart.cache_name, chart)
        return render(request, 'resultados/resultados.html', {'eleccion': eleccion, 'header': header,
                                                'totales':dict_totales.values(), 'agrupaciones': agrupaciones,
                                                'votos': votos, 'conteos': conteos,
                                                'municipios': municipios, 'chart': chart,
                                                'refresh': 120,})

def ranking(request, eleccion_id, cargo_id, municipio_id=None):
    eleccion = Eleccion.objects.get(pk=eleccion_id)
    actualizar_resultados(eleccion)
    cargo = Cargo.objects.get(pk=cargo_id)
    listas = Lista.objects.select_related('agrupacion', 'municipio').filter(cargos=cargo)
    votos = Voto_Resumen.objects.select_related('lista', 'municipio').filter(cargo=cargo, lista__in=listas)
    cant_cargos = CantCargos.objects.filter(cargo=cargo)
    #Chart
    chart = Charts()
    chart.table= []#PREGUNTARLE A FISA XQ si el objeto esta recien creado trae datos viejos (?)!!!!!
    chart.cache_name += 'ranking-'
    chart.cargo = Cargo.objects.get(pk=cargo_id)
    chart.titulo += ' para ' + chart.cargo.nombre

    if municipio_id is not None:#En caso de que manden municipio
        municipio = Municipio.objects.get(pk=municipio_id)
        chart.titulo+= ' (Municipal)'
        listas = listas.filter(municipio=municipio)
        votos = votos.filter(municipio=municipio)
        if cant_cargos.filter(municipio=municipio):
            cant_cargos = cant_cargos.get(municipio=municipio)
        else:
            cant_cargos = cant_cargos.first()
    else:
        municipio = None
        cant_cargos = cant_cargos.first()
        chart.titulo+= ' (Provincial)'

    
    if request.path[:9] == '/rankpro/':
        chart.titulo+= ' Proyectado'
        votos = proyectar(eleccion, [v for v in votos], municipio)[0]
    elif request.path[:14] == '/rankprobruto/':
        chart.titulo+= ' Proyectado BRUTO'
        votos = proyectar_bruto(eleccion, [v for v in votos], municipio)[0]

    for lista in listas:#Calculamos los totales
        lista.total_votos = 0
        for v in votos:
            if v.lista == lista:
                lista.total_votos += v.cantidad

    if not municipio:
        agrupaciones = Agrupacion.objects.filter(eleccion=eleccion)
        for agrupacion in agrupaciones:
            agrupacion.total_votos = 0
            for lista in listas:
                if lista.agrupacion == agrupacion:
                    agrupacion.total_votos += lista.total_votos
        listas = agrupaciones        
    #Iniciamos el proceso
    piso = 0
    cargo.cant_escanios = cant_cargos.cant_escanios
    full_rank = []
    eliminados = []
    if cargo.poder == 1:#Ejecutivo, Solo un ganador
        for lista in listas:
            if lista.total_votos > 0:
                full_rank.append((lista.total_votos, lista))
    elif cargo.poder == 2:#Legislativo, aplica Dhont
        listas_validas = []
        #Obtenemos el total de votantes
        cant_votantes = obtener_cvotantes(eleccion, municipio)
        piso = int(cant_votantes * 0.05)
        #Discriminamos las que no alcanzan el piso
        for lista in listas:
            if piso < lista.total_votos:
                listas_validas.append(lista)
            else:
                if lista.total_votos > 0:
                    eliminados.append((lista.total_votos,lista))
        #Generamos las cuotas
        for lista_valida in listas_validas:
            index = 0
            while index < cargo.cant_escanios:
                index+=1
                full_rank.append((int(lista_valida.total_votos / index), lista_valida))

    full_rank.sort(key=itemgetter(0), reverse=True)
    eliminados.sort(key=itemgetter(0), reverse=True)
    full_rank= [full_rank[:cargo.cant_escanios], full_rank[cargo.cant_escanios:]]
    
    top_rank = {}
    #charts:
    if cargo.poder == 1:#Ejecutivo
        chart.table.append([full_rank[0][0][1].nombre, full_rank[0][0][0]])#El ganador
        for item in full_rank[1][:5]:
            chart.table.append([item[1].nombre, item[0]])
        chart.table = [['Partido', 'Votos']] + chart.table
    elif cargo.poder == 2:#Legislativo, aplica Dhont
        for ganador in full_rank[0]:
            if ganador[1].nombre in top_rank:
                top_rank[ganador[1].nombre] += 1
            else:
                top_rank[ganador[1].nombre] = 1
        if eliminados:
            full_rank.append(eliminados)
        #charts:    
        for item in top_rank:
            chart.table.append([item, top_rank[item]])
        chart.table = [['Partido', 'Cargos']] + chart.table

    chart.cache_name += str(eleccion.id)+'-'+str(chart.cargo.id)
    if municipio:
        chart.cache_name += '-'+str(municipio.id)
    cache.set(chart.cache_name, chart)
    return render(request, 'resultados/ranking.html', {'eleccion': eleccion, 'cargo': cargo, 'municipio': municipio, 'chart': chart,
                                                       'full_rank': full_rank, 'piso': piso, 'top_rank':top_rank, 'refresh': 120,})

#SITUACION
@staff_member_required
def mesas_cargadas(request, eleccion_id, municipio_id=None):
    if request.method == 'POST':
        return redirect('core:mesas_cargadas', eleccion_id=eleccion_id, municipio_id=request.POST["id_municipio"])

    eleccion = Eleccion.objects.get(pk= eleccion_id)
    municipios = Municipio.objects.filter(seccion__provincia__eleccion=eleccion).order_by('alpha_id')
    mesas = Mesa.objects.select_related('acta', 'escuela', 'escuela__circuito', 'escuela__circuito__municipio').filter(escuela__circuito__municipio__in=municipios).order_by('numero')
    
    #chart:
    chart = Charts()
    chart.table= []
    chart.titulo = 'Situacion Escrutinio'
    chart.cache_name += '_mesascargadas-'+ str(eleccion.id)
    
    if municipio_id is not None:
        municipio = Municipio.objects.get(pk=municipio_id)
        mesas = mesas.filter(escuela__circuito__municipio=municipio)
        chart.titulo += ' para ' + municipio.nombre
    else:
        municipio = None
    #Aqui procesamos para ver que mesas estan cargadas y cuales no
    cmesas, escrutadas = 0, 0#For chart
    votos = Voto.objects.filter(mesa__in=mesas).values('mesa__numero').distinct()
    cmesas += len(mesas)#For chart
    for mesa in mesas:
        if {'mesa__numero': mesa.numero} in votos:
            mesa.cargada = True
            escrutadas += 1#For chart
        else:
            mesa.cargada = False
    chart.table = [['Tipo', 'Cantidad'],['Escrutadas', escrutadas],['Sin Escrutar', cmesas-escrutadas]]
    if municipio:
        chart.cache_name += '-'+str(municipio.id)
    cache.set(chart.cache_name, chart)
    return render(request, 'mesas/mesas_cargadas.html', {'eleccion': eleccion, 'municipios': municipios, 'municipio': municipio,
                                                         'mesas': mesas, 'chart': chart, 'refresh': 120, })

def listado_mesas_csv(request, eleccion_id):
    eleccion = Eleccion.objects.get(pk=eleccion_id)
    mesas = Mesa.objects.filter(escuela__circuito__municipio__seccion__provincia__eleccion=eleccion).order_by('numero')
    mesas = mesas.select_related('acta', 'escuela', 'escuela__circuito', 'escuela__circuito__municipio')
    votos = Voto.objects.filter(mesa__in=mesas).values('mesa__numero').distinct()
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="listado_mesas.csv"'
    writer = csv.writer(response)
    writer.writerow(['Num_Mesa', 'id Municipio', 'Municipio','id Circuito','Circuito', 'Escuela', 'Testigo', 'Cargada', 'Digitalizada'])
    for mesa in mesas:
        writer.writerow([mesa.numero, 
                        mesa.escuela.circuito.municipio.alpha_id, mesa.escuela.circuito.municipio.nombre,
                        mesa.escuela.circuito.alpha_id, mesa.escuela.circuito.nombre,
                        mesa.escuela.nombre,
                        mesa.testigo, {'mesa__numero': mesa.numero} in votos, mesa.acta.escaneado])
    return response

@staff_member_required
def situacion_escrutinio(request, eleccion_id, municipio_id=None): # RE ESCRIBIRLA
    if request.method == 'POST':
        if "id_municipio" in request.POST:
            return redirect('core:situacion_escrutinio', eleccion_id=eleccion_id, municipio_id=request.POST["id_municipio"])    
    #chart:
    chart = Charts()
    chart.table= []#PREGUNTARLE A FISA XQ si el objeto esta recien creado trae datos viejos (?)!!!!!
    chart.titulo = 'Situacion Escrutinio'

    listado = []
    eleccion = Eleccion.objects.get(pk=eleccion_id)
    municipios = Municipio.objects.filter(seccion__provincia__eleccion=eleccion).order_by('alpha_id')
    mesas = Mesa.objects.filter(escuela__circuito__municipio__in=municipios).select_related('acta', 'escuela', 'escuela__circuito', 'escuela__circuito__municipio')

    if municipio_id:
        municipio = municipios.get(pk=municipio_id)
        chart.titulo += ' para ' + municipio.nombre
        circuitos = Circuito.objects.filter(municipio=municipio).order_by('alpha_id')
        circuitos = circuitos.select_related('municipio').prefetch_related('escuelas')
        mesas = mesas.filter(escuela__circuito__municipio=municipio)
    else:
        municipio = None
        circuitos = None
    
    votos = {v.mesa.numero : v for v in Voto.objects.filter(mesa__in=mesas).select_related('mesa', 'mesa__acta', 'mesa__escuela', 'mesa__escuela__circuito', 'mesa__escuela__circuito__municipio')}

    cmesas, escrutadas = 0, 0#For chart
    if municipio is None:
        for muni in municipios:
            item = Items()
            item.alpha_id = muni.alpha_id
            item.nombre = muni.nombre
            item.cmesas = sum([1 for m in mesas if m.escuela.circuito.municipio == muni])
            item.cescrutadas = sum([1 for v in votos.values() if v.mesa.numero in votos and v.mesa.escuela.circuito.municipio == muni])
            item.cvotantes = sum([m.acta.celectores for m in mesas if m.escuela.circuito.municipio == muni])
            item.mesa_inicio = min([m.numero for m in mesas if m.escuela.circuito.municipio == muni])
            item.mesa_fin = item.mesa_inicio + item.cmesas - 1 #Dada las extranjeras fuera de circuito debemos hacerlo asi. no usando max()
            item.porcentaje = (item.cescrutadas / item.cmesas) * 100  # y formato (decimales, %, etc) se da en el template
            listado.append(item)
            cmesas += item.cmesas#For chart
            escrutadas += item.cescrutadas#For chart
    else:
        for circ in circuitos:
            item = Items()
            item.alpha_id = circ.alpha_id
            item.nombre = circ.nombre
            item.cmesas = sum([1 for m in mesas if m.escuela.circuito == circ])
            item.cescrutadas = sum([1 for v in votos.values() if v.mesa.numero in votos and v.mesa.escuela.circuito == circ])
            item.cvotantes = sum([m.acta.celectores for m in mesas if m.escuela.circuito == circ])
            item.mesa_inicio = min([m.numero for m in mesas if m.escuela.circuito == circ])
            item.mesa_fin = item.mesa_inicio + item.cmesas - 1 #Dada las extranjeras fuera de circuito debemos hacerlo asi. no usando max()
            item.porcentaje = (item.cescrutadas / item.cmesas) * 100  # y formato (decimales, %, etc) se da en el template
            listado.append(item)
            cmesas += item.cmesas#For chart
            escrutadas += item.cescrutadas#For chart

    chart.table= [['Tipo', 'Cantidad'],['Escrutadas', escrutadas],['Sin Escrutar', cmesas-escrutadas]]
    chart.cache_name += '_situ_mesas-' + str(eleccion.id)
    if municipio:
        chart.cache_name += '-'+str(municipio.id)
    cache.set(chart.cache_name, chart)
    return render(request, 'mesas/mesas_situacion.html', {'eleccion': eleccion, 'municipios': municipios, 'municipio': municipio,
                                                        'circuitos': circuitos, 'listado' : listado, 'chart': chart,
                                                        'refresh': 120, })

@staff_member_required
def grafico(request, cache_name):
    chart = cache.get(cache_name)
    if chart:#Si no existe!
        chart.width = 1100
        chart.height = 800
    else:
        chart=None
    return render(request, 'resultados/grafico.html', {'chart': chart})

#FISCALES
#   CONTROLES
@staff_member_required
def control_diferencial(request):
    if request.method == 'GET':
        form = CruceDiferencial()
        return render(request, 'utils/generic_form.html', {'form': form, 'Titulo': 'Sistema de control Diferencial',})
    else:
        form = CruceDiferencial(request.POST)
        if form.is_valid():
            eleccion1 = form.cleaned_data['eleccion1']
            eleccion2 = form.cleaned_data['eleccion2']
            valor_minimo = form.cleaned_data['diff_minima']
            cargos1 = Cargo.objects.filter(eleccion=eleccion1, orden__in=form.cleaned_data['cargos'])
            cargos2 = Cargo.objects.filter(eleccion=eleccion2, orden__in=form.cleaned_data['cargos'])
            if form.cleaned_data['municipio1']:
                municipio1 = form.cleaned_data['municipio1']
                municipio2 = Municipio.objects.get(alpha_id=form.cleaned_data['municipio1'].alpha_id, seccion__provincia__eleccion=eleccion2)
                listas1 = Lista.objects.select_related('agrupacion').filter(municipio=municipio1, agrupacion__alpha_id__in=form.cleaned_data['agrupaciones'])
                listas2 = Lista.objects.select_related('agrupacion').filter(municipio=municipio2, agrupacion__alpha_id__in=form.cleaned_data['agrupaciones'])
                votos1 = {str(v.mesa.numero)+'-'+str(v.cargo.orden)+'-'+str(v.lista.agrupacion.alpha_id) : v for v in Voto.objects.filter(lista__in=listas1, cargo__in=cargos1).select_related('mesa', 'cargo', 'lista', 'lista__agrupacion').order_by('mesa__numero')}
                votos2 = {str(v.mesa.numero)+'-'+str(v.cargo.orden)+'-'+str(v.lista.agrupacion.alpha_id) : v for v in Voto.objects.filter(lista__in=listas2, cargo__in=cargos2).select_related('mesa', 'cargo', 'lista', 'lista__agrupacion').order_by('mesa__numero')}
            else:
                municipio1 = None
                agrupaciones1 = Agrupacion.objects.filter(eleccion=eleccion1)
                listas1 = agrupaciones1
                agrupaciones2 = Agrupacion.objects.filter(eleccion=eleccion2)
                votos1 = {str(v.mesa.numero)+'-'+str(v.cargo.orden)+'-'+str(v.lista.agrupacion.alpha_id) : v for v in Voto.objects.filter(lista__agrupacion__in=agrupaciones1, cargo__in=cargos1)}
                votos2 = {str(v.mesa.numero)+'-'+str(v.cargo.orden)+'-'+str(v.lista.agrupacion.alpha_id) : v for v in Voto.objects.filter(lista__agrupacion__in=agrupaciones2, cargo__in=cargos2)}
            
            reporte = []
            for v1 in votos1.values():
                if str(v1.mesa.numero)+'-'+str(v1.cargo.orden)+'-'+str(v1.lista.agrupacion.alpha_id) in votos2:
                    v2 = votos2[str(v1.mesa.numero)+'-'+str(v1.cargo.orden)+'-'+str(v1.lista.agrupacion.alpha_id)].cantidad
                    diff = v1.cantidad - v2 
                    if abs(diff) > valor_minimo:
                        reporte.append((v1.mesa.numero, v1.lista.agrupacion.alpha_id, v1.lista.agrupacion.nombre, v1.cargo, v1.cantidad, v2, diff))
                else:
                    reporte.append((v1.mesa.numero, v1.lista.agrupacion.alpha_id, v1.lista.agrupacion.nombre, v1.cargo, v1.cantidad, 'Sin Cargar', 0))
            
            for v2 in votos2.values():
                if str(v2.mesa.numero)+'-'+str(v2.cargo.orden)+'-'+str(v2.lista.agrupacion.alpha_id) not in votos1:
                    reporte.append((v2.mesa.numero, v2.lista.agrupacion.alpha_id, v2.lista.agrupacion.nombre, v1.cargo, 'Sin Cargar', v2.cantidad, 0))
            
            if form.cleaned_data['download'] == True:
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment; filename="control_diferencial.csv"'
                writer = csv.writer(response)
                writer.writerow(['CONTROL DIFERENCIAL'])
                if municipio1:
                    writer.writerow([municipio1.alpha_di, municipio1.nombre])
                else:
                    writer.writerow(['Provincial'])
                writer.writerow(['Para cargos: ']+[c.nombre for c in cargos1])
                writer.writerow(['Mesa', 'id Agrupacion', 'Nombre Agrupacion', 'Cargo', 'Cant. Nuestra' ,'Cant. Simecom', 'Diferencia'])
                for linea in reporte:
                    writer.writerow([linea[0], linea[1], linea[2], linea[3], linea[4], linea[5], linea[6]])
                return response                
            else:
                return render(request, 'controles/reporte_diferencial.html', {'eleccion1': eleccion1, 'eleccion2': eleccion2, 'municipio': municipio1, 'listas': listas1, 'cargos': cargos1, 'reporte': reporte, })        
        else:
            message = form.errors
            return render(request, "extras/upload_csv.html", {'message': message, 'form': form,})

@staff_member_required
def control_estadistico(request):
    if request.method == 'GET':
        form = ControlEstadistico()
        return render(request, 'utils/generic_form.html', {'form': form, 'Titulo': 'Sistema de control Estadistica',})
    else:
        form = ControlEstadistico(request.POST)
        if form.is_valid():
            #Obtenemos todos los datos del form
            eleccion = form.cleaned_data['eleccion']
            municipio = form.cleaned_data['municipio']
            cargos = Cargo.objects.filter(eleccion=eleccion, orden__in=form.cleaned_data['cargos'])
            agrupaciones = Agrupacion.objects.filter(eleccion=eleccion, alpha_id__in=form.cleaned_data['agrupaciones'])
            porc_minimo = form.cleaned_data['diff_minima']
            #Obtenemos votos y los filtramos
            votos = Voto.objects.filter(mesa__escuela__circuito__municipio=municipio)
            votos = votos.filter(cargo__in=cargos)
            votos = votos.filter(lista__agrupacion__in=agrupaciones)
            votos = votos.select_related('mesa', 'cargo', 'lista', 'lista__agrupacion').order_by('mesa__numero')
            #Obtenemos los totales por mesa y cargo
            totales = Conteo.objects.filter(total__totales=True, cargo__in=cargos)
            totales = totales.filter(mesa__escuela__circuito__municipio=municipio)
            totales = totales.select_related('mesa', 'cargo')
            totales_mesa = {(t.mesa , t.cargo): t.cantidad for t in totales}
            totales_cargo = {}
            for t in totales:
                if t.cargo in totales_cargo:
                    totales_cargo[t.cargo] += t.cantidad
                else:
                    totales_cargo[t.cargo] = t.cantidad          
            #Construimos Estadisticos generales
            votos_listas = {}
            for v in votos:
                if (v.lista.agrupacion, v.cargo) in votos_listas:
                    votos_listas[(v.lista.agrupacion, v.cargo)] += v.cantidad
                else:
                    votos_listas[(v.lista.agrupacion, v.cargo)] = v.cantidad

            porc_listas = {}
            for v in votos_listas:
                porc_listas[v] = (100.0 / totales_cargo[v[1]]) * votos_listas[v]
            
            #Analizamos cada mesa por separado y generamos reporte
            reporte = []
            for v in votos:
                porc_voto = (100.0 / totales_mesa[(v.mesa, v.cargo)]) * v.cantidad
                diff = porc_voto - porc_listas[(v.lista.agrupacion, v.cargo)]
                if abs(diff) > porc_minimo:
                    reporte.append([v.mesa.numero, v.lista.agrupacion.alpha_id, v.lista.nombre, v.cargo.nombre, v.cantidad, totales_mesa[v.mesa,v.cargo], porc_voto, porc_listas[(v.lista.agrupacion, v.cargo)], diff])

            if form.cleaned_data['download'] == True:
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment; filename="control_estadistico.csv"'
                writer = csv.writer(response)
                writer.writerow(['CONTROL ESTADISTICO'])
                writer.writerow(['Municipio:' ,municipio.alpha_id, municipio.nombre])
                writer.writerow(['Para cargos: ']+[c.nombre for c in cargos])
                writer.writerow(['Mesa', 'id Agrupacion', 'Nombre Agrupacion', 'Cargo', 'Cant. Votos' ,'Total Votos', 'Porcentaje Acta', 'Porcentaje General', 'Diff'])
                for linea in reporte:
                    writer.writerow([linea[0], linea[1], linea[2], linea[3], linea[4], linea[5], linea[6], linea[7], linea[8]])
                return response   
            else:
                return render(request, 'controles/reporte_estadistico.html', {'eleccion': eleccion, 'municipio': municipio, 'agrupaciones': agrupaciones, 'cargos': cargos, 'reporte': reporte, })
        else:
            message = form.errors
            return render(request, "extras/upload_csv.html", {'message': message, 'form': form,})

@staff_member_required
def control_nopositivos(request):
    if request.method == 'GET':
        form = ControlNoPositivos()
        return render(request, 'utils/generic_form.html', {'form': form, 'Titulo': 'Sistema de control de Votos No positivos',})
    else:
        form = ControlNoPositivos(request.POST)
        if form.is_valid():
            #Obtenemos todos los datos del form
            eleccion = form.cleaned_data['eleccion']
            municipio = form.cleaned_data['municipio']
            municipio = Municipio.objects.get(seccion__provincia__eleccion=eleccion, alpha_id=municipio.alpha_id)
            cargos = Cargo.objects.filter(eleccion=eleccion, orden__in=form.cleaned_data['cargos'])
            totales = Total.objects.filter(eleccion=eleccion, orden__in=form.cleaned_data['totales'])
            valor_maximo = form.cleaned_data['maximo']
            #Obtenemos votos y los filtramos
            conteos = Conteo.objects.filter(mesa__escuela__circuito__municipio__seccion__provincia__eleccion=eleccion)
            if municipio:
                conteos = conteos.filter(mesa__escuela__circuito__municipio=municipio)
            conteos = conteos.filter(cargo__in=cargos)
            conteos = conteos.filter(total__in=totales)
            conteos = conteos.filter(cantidad__gt=valor_maximo).order_by('mesa__numero')
            #Generamos Reporte
            reporte = []
            for c in conteos:
                reporte.append([c.mesa.numero, c.total.nombre, c.cargo.nombre, c.cantidad])
            #Si pidieron descargar
            if form.cleaned_data['download'] == True:
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment; filename="control_estadistico.csv"'
                writer = csv.writer(response)
                writer.writerow(['CONTROL DE VOTOS NO POSITIVOS'])
                if municipio:
                    writer.writerow(['Municipio:' ,municipio.alpha_id, municipio.nombre])
                else:
                    writer.writerow(['Provincial'])
                writer.writerow(['Para cargos: ']+[c.nombre for c in cargos])
                writer.writerow(['Para tipos: ']+[t.nombre for t in totales])
                writer.writerow(['Valor Maximo Aceptable: ']+[valor_maximo])
                writer.writerow(['Mesa', 'Tipo', 'Cargo', 'Cant. Votos'])
                for linea in reporte:
                    writer.writerow([linea[0], linea[1], linea[2], linea[3]])
                return response   
            else:
                return render(request, 'controles/reporte_nopositivos.html', {'eleccion': eleccion, 'municipio': municipio, 'totales': totales, 'cargos': cargos, 'valor_maximo': valor_maximo, 'reporte': reporte, })
        else:
            message = form.errors
            return render(request, "extras/upload_csv.html", {'message': message, 'form': form,})

@staff_member_required
def control_limites(request):
    if request.method == 'GET':
        form = ControlxLimites()
        return render(request, 'utils/generic_form.html', {'form': form, 'Titulo': 'Sistema de control de Votos No positivos',})
    else:
        form = ControlxLimites(request.POST)
        if form.is_valid():
            #Obtenemos todos los datos del form
            eleccion = form.cleaned_data['eleccion']
            cargos = Cargo.objects.filter(eleccion=eleccion, orden__in=form.cleaned_data['cargos'])
            agrupaciones = Agrupacion.objects.filter(eleccion=eleccion, alpha_id__in=form.cleaned_data['agrupaciones'])
            valor_minimo = form.cleaned_data['minimo']
            valor_maximo = form.cleaned_data['maximo']
            #Obtenemos Votos
            votos = Voto.objects.filter(mesa__escuela__circuito__municipio__seccion__provincia__eleccion=eleccion)
            votos = votos.filter(cargo__in=cargos)
            votos = votos.select_related('mesa', 'cargo', 'lista', 'lista__agrupacion')
            if form.cleaned_data['municipio']:
                municipio = Municipio.objects.get(seccion__provincia__eleccion=eleccion, alpha_id=form.cleaned_data['municipio'].alpha_id)
                votos = votos.filter(mesa__escuela__circuito__municipio=municipio)
            else:
                municipio = None
            votos = votos.filter(lista__agrupacion__in=agrupaciones)
            #Aplicamos filtro de limites
            votos = votos.exclude(cantidad__range=(valor_minimo,valor_maximo)).order_by('mesa__numero')
            #Cargamos el reporte
            reporte = []
            for v in votos:
                reporte.append([v.mesa.numero, v.lista.agrupacion.alpha_id, v.lista.nombre, v.cargo.nombre, v.cantidad])
            #Si pidieron descargar
            if form.cleaned_data['download'] == True:
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment; filename="control_limites.csv"'
                writer = csv.writer(response)
                writer.writerow(['CONTROL DE VOTOS POR LIMITES'])
                if municipio:
                    writer.writerow(['Municipio:' ,municipio.alpha_id, municipio.nombre])
                else:
                    writer.writerow(['Provincial'])
                writer.writerow(['Para cargos: ']+[c.nombre for c in cargos])
                writer.writerow(['Para Partido: ']+[str(a.alpha_id) + ' - ' + a.nombre for a in agrupaciones])
                writer.writerow(['Para limites:', 'minimo: '+str(valor_minimo), 'maximo: '+str(valor_maximo)])
                writer.writerow(['Mesa', 'Lista ID', 'Lista Nombre', 'Cargo', 'Cant. Votos'])
                for linea in reporte:
                    writer.writerow([linea[0], linea[1], linea[2], linea[3], linea[4]])
                return response   
            else:
                return render(request, 'controles/reporte_limites.html', {'eleccion': eleccion, 'municipio': municipio, 
                                                                          'valor_minimo': valor_minimo, 'valor_maximo': valor_maximo,
                                                                          'listas': agrupaciones, 'cargos': cargos, 'reporte': reporte, })


@staff_member_required
def verresponsables(request, eleccion_id, municipio_id=None):
    if request.method == 'POST':
        return redirect('core:verresponsables', eleccion_id=eleccion_id, municipio_id=request.POST["id_municipio"]) 
    eleccion = Eleccion.objects.get(pk=eleccion_id)
    municipios = Municipio.objects.filter(seccion__provincia__eleccion=eleccion).order_by('alpha_id')
    responsables = Responsable.objects.select_related('mesa', 'escuela', 'circuito', 'municipio').filter(eleccion=eleccion, autorizado=True).all()
    if municipio_id:
        municipio = Municipio.objects.get(pk=municipio_id)
        responsables = responsables.filter(municipio=municipio)
        responsables = responsables | Responsable.objects.filter(circuito__municipio=municipio)
        responsables = responsables | Responsable.objects.filter(escuela__circuito__municipio=municipio)
        responsables = responsables | Responsable.objects.filter(mesa__escuela__circuito__municipio=municipio)
    else:
        municipio= None
    return render(request, 'responsables/responsables.html', {'eleccion': eleccion, 'municipios': municipios, 'municipio': municipio,
                                                 'responsables': responsables,})

@staff_member_required
def cargarresponsable(request, eleccion_id):
    eleccion = Eleccion.objects.get(pk=eleccion_id)
    form = ResponsableForm(initial={'eleccion': eleccion})
    guardado = False
    if request.method == 'POST':
        form = ResponsableForm(request.POST)
        if form.is_valid:
            form.save()
            guardado = True
    return render(request, 'utils/generic_form.html', {'form': form, 'guardado': guardado,})

@staff_member_required
def replicar_actas(request, eleccion_id):
    guardado = False
    eleccion = Eleccion.objects.get(pk=eleccion_id)
    if request.method == 'POST' and request.user.is_authenticated:
        form = ReplicarActasForm(request.POST)
        if form.is_valid():
            guardado = True
            municipio_origen = form.cleaned_data.get('municipio_origen')
            municipio_destino = form.cleaned_data.get('municipio_destino')
            guardado = replicar_listas(municipio_origen, municipio_destino)
    else:
        form = ReplicarActasForm(eleccion_id=eleccion.id)
    return render(request, 'utils/generic_form.html', {'form': form, 'guardado': guardado,})

@superuser_required
def replicar_eleccion(request):
    titulo = "Replica de Elecciones"
    if request.method == "POST":
        form = ReplicarEleccionForm(request.POST, request.FILES)
        if form.is_valid():
            eleccion_vieja = form.cleaned_data['eleccion_vieja']
            eleccion_nueva = form.cleaned_data['eleccion_nueva']
            provincia_vieja = form.cleaned_data['provincia_vieja']
            provincia_nueva = Provincia.objects.get_or_create(eleccion=eleccion_nueva, nombre=provincia_vieja.nombre)[0]
            print("Eleccion Lista")
            #Cargamos Info Geografica
            if form.cleaned_data['csvgeo']:
                print("Iniciando carga Geografica")
                csvgeo = form.cleaned_data['csvgeo']
                file_data = csvgeo.read().decode("utf-8")
                lines = file_data.split("\n")
                cargar_geografico(provincia_nueva, lines)
            #Cargamos info de Mesas
            if form.cleaned_data['csvmesas']:
                print("Iniciando carga de Mesas")
                csvmesas = form.cleaned_data['csvmesas']
                file_data = csvmesas.read().decode("utf-8")
                lines = file_data.split("\n")
                cargar_mesas(provincia_nueva, lines)
            if form.cleaned_data['bool_parametrizar'] == True:
                # COPIAR COLUMNAS, CARGOS, TOTALES, AGRUPACIONES
                Columna.objects.filter(eleccion=eleccion_nueva).delete()
                for item in eleccion_vieja.columnas.all():
                    item.id = None
                    item.eleccion = eleccion_nueva
                    item.save()
                print("Columnas Copiadas")
                Cargo.objects.filter(eleccion=eleccion_nueva).delete()
                for item in eleccion_vieja.cargos.all():
                    item.id = None
                    item.eleccion = eleccion_nueva
                    item.save()
                print("Cargos Copiadas")
                Total.objects.filter(eleccion=eleccion_nueva).delete()
                for item in eleccion_vieja.totales.all():
                    item.id = None
                    item.eleccion = eleccion_nueva
                    item.save()
                print("Totales Copiados")
            if form.cleaned_data['bool_partidos'] == True:
                Agrupacion.objects.filter(eleccion=eleccion_nueva).delete()
                for item in eleccion_vieja.agrupaciones.all():
                    item.id = None
                    item.eleccion = eleccion_nueva
                    item.save()
                print("Agrupaciones Copiadas")
                # Copiar LISTAS
                Lista.objects.filter(agrupacion__eleccion=eleccion_nueva).delete()
                municipios_nuevos = {m.alpha_id : m for m in Municipio.objects.filter(seccion__provincia__eleccion=eleccion_nueva)}
                agrupaciones_nuevas = {a.alpha_id : a for a in Agrupacion.objects.filter(eleccion= eleccion_nueva)}
                dict_cargos_nuevos = {c.orden : c for c in Cargo.objects.filter(eleccion=eleccion_nueva)}
                for agrupacion in eleccion_vieja.agrupaciones.all():
                    for lista in agrupacion.listas.all():
                        lista_nueva = Lista()
                        lista_nueva.agrupacion = agrupaciones_nuevas[lista.agrupacion.alpha_id]
                        lista_nueva.municipio = municipios_nuevos[lista.municipio.alpha_id]
                        lista_nueva.save()
                        for lc in lista.cargos.all():
                            lista_nueva.cargos.add(dict_cargos_nuevos[lc.orden])#CHEQUEAR ESTO!
                        lista_nueva.save()
                    print("Listas copiadas para: " + str(agrupacion.alpha_id) + ' ' + agrupacion.nombre)
                print("TODAS LAS LISTAS COPIADAS")
                # COPIAR CANT_CARGOS (desde provincia y municipios)
                CantCargos.objects.filter(cargo__in=Cargo.objects.filter(eleccion=eleccion_nueva)).delete()
                for cantcargos in CantCargos.objects.filter(provincia=provincia_vieja):
                    cantcargos.id = None
                    cantcargos.cargo = dict_cargos_nuevos[cantcargos.cargo.orden]
                    if cantcargos.provincia: 
                        cantcargos.provincia = provincia_nueva 
                    if cantcargos.municipio:
                        cantcargos.municipio = municipios_nuevos[cantcargos.municipio.alpha_id]
                    cantcargos.save()
                print("CANTCARGOS COPIADOS")
            return render(request, 'extras/upload_csv.html', {'count': 1, })
        else:
            message = form.errors
            return render(request, "extras/upload_csv.html", {'message': message, })
    else:
        form = ReplicarEleccionForm()
    return render(request, "extras/upload_csv.html", {'titulo': titulo, 'form': form,})

#consistencia
def rellenar_totales(request, eleccion_id):
    eleccion = Eleccion.objects.get(pk=eleccion_id)
    mesas = Mesa.objects.filter(escuela__circuito__municipio__seccion__provincia__eleccion=eleccion)
    cargos = Cargo.objects.filter(eleccion=eleccion)
    dict_votos = {}
    for v in Voto.objects.filter(mesa__in=mesas).select_related('mesa', 'cargo', 'lista'):
        if (v.mesa,v.cargo) in dict_votos:
            dict_votos[(v.mesa,v.cargo)] += v.cantidad
        else:
            dict_votos[(v.mesa,v.cargo)] = v.cantidad
    for c in Conteo.objects.filter(mesa__in=mesas, total__totales=False).select_related('mesa', 'cargo'):
        if (c.mesa,c.cargo) in dict_votos:
            dict_votos[(c.mesa,c.cargo)] += c.cantidad
        else:
            dict_votos[(c.mesa,c.cargo)] = c.cantidad

    total = Total.objects.filter(eleccion=eleccion, totales=True).first()
    dict_conteos = {}
    for c in Conteo.objects.filter(mesa__in=mesas, total=total).select_related('mesa', 'cargo'):
        dict_conteos[(c.mesa,c.cargo)] = c.cantidad

    cant_mesas = 0
    totales = []
    for mesa in mesas:
        for cargo in cargos:
            if (mesa, cargo) not in dict_conteos:
                try:
                    t = dict_votos[(mesa,cargo)]
                    print("Generamos totales para: " + str(mesa.numero))
                    totales.append(Conteo(mesa=mesa, total=total, cargo=cargo, cantidad=t, rellenado=True))
                    cant_mesas += 1
                except:
                    pass
    if cant_mesas:
        Conteo.objects.bulk_create(totales)
        Voto_Resumen.objects.all().delete()
        Conteo_Resumen.objects.all().delete()
        actualizar_resultados(eleccion)
    message = "Se corrigieron " + str(cant_mesas) + " mesas que no tenian totales."
    return render(request, 'extras/upload_csv.html', {'message': message, })

#CARGAS MASIVAS
@superuser_required
def upload_csv(request, tipo=''):
    titulo = "Carga de "+tipo
    if request.method == "GET":
        if tipo == '':
            titulo = "Sistema de Carga Masiva"
            return render(request, "extras/upload_csv.html", {'titulo': titulo, })
        else:
            form = UploadCsv()
            if tipo=='padron':
                form = UploadPadron()
            elif tipo=='cargar_votos':
                form = UploadVotosForm()
            elif tipo=='mesas_testigos':
                form = UploadMesasTestigo()
            return render(request, "extras/upload_csv.html", {'titulo': titulo, 'form': form,})
    else:#Si subieron un archivo
        if tipo=='padron':
            form = UploadPadron(request.POST, request.FILES)
        elif tipo=='cargar_votos':
            form = UploadVotosForm(request.POST, request.FILES)
        elif tipo=='mesas_testigos':
            form = UploadMesasTestigo(request.POST, request.FILES)
        else:
            form = UploadCsv(request.POST, request.FILES)
        if form.is_valid():
            eleccion = form.cleaned_data['eleccion']
            csv_file = form.cleaned_data['csvfile']
            count = 0
            file_data = csv_file.read().decode("utf-8")
            lines = file_data.split("\n")
            count = len(lines)
            #Diferentes tipos de cargas
            if tipo == 'geografica':
                cargar_geografico(form.cleaned_data['provincia'], lines)
                return render(request, 'extras/upload_csv.html', {'count': len(lines), })
            elif tipo == 'mesas':
                cargar_mesas(form.cleaned_data['provincia'], lines)
                return render(request, 'extras/upload_csv.html', {'count': len(lines), })
            elif tipo == 'mesas_testigos':
                #ESTO NO ESTA HECHO FALTA AGREGAR CAMPOS, ACTUALIZAR, ETC
                padron = form.cleaned_data['padron']
                if padron.actual:
                    mesas_mod = None
                else:
                    mesas_mod = actualizar_testigos(padron)
                return render(request, 'extras/upload_csv.html', {'count': len(lines), 'mesas_mod': mesas_mod, })
            elif tipo == 'padron':
                cargar_padron(form.cleaned_data['provincia'], form.cleaned_data['fecha'], form.cleaned_data['descripcion'], lines)
                return render(request, 'extras/upload_csv.html', {'count': len(lines), })
            elif tipo == 'cargar_votos':
                cargar_votos(form.cleaned_data['eleccion'], form.cleaned_data['municipio_destino'], lines)
                return render(request, 'extras/upload_csv.html', {'count': len(lines), })
            else:
                message = 'No Existe ese tipo de carga masiva.'
                return render(request, "extras/upload_csv.html", {'message': message, })
        else:
            message = form.errors
            return render(request, "extras/upload_csv.html", {'message': message, })

@superuser_required
def download_oficiales(request):
    if request.method == "GET":
        titulo = "Sistema de carga masiva del escrutinio oficial"
        form = DownloadOficialesForm()
        return render(request, "extras/upload_csv.html", {'titulo': titulo, 'form': form,})
    else:
        form = DownloadOficialesForm(request.POST)
        if form.is_valid():
            eleccion = form.cleaned_data['eleccion_destino']
            usuario = form.cleaned_data['usuario']
            password = form.cleaned_data['password']
            mesa_inicio = form.cleaned_data['mesa_inicio']
            mesa_fin = form.cleaned_data['mesa_fin']
            link = form.cleaned_data['link']
            cant = 0

            new_queue = crear_progress_link(str("Cargar_Mesas"))#AGREGAR FECHA
            while mesa_inicio < mesa_fin:
                download_mesas(eleccion.id, link, usuario, password, mesa_inicio, mesa_inicio+100, schedule=int(mesa_inicio/10), queue=new_queue)
                mesa_inicio+=100
            download_mesas(eleccion.id, link, usuario, password, mesa_inicio+1, mesa_fin, schedule=int(mesa_inicio/10), queue=new_queue)
            return redirect('core:task_progress', new_queue)
        else:
            message = form.errors
            return render(request, "extras/upload_csv.html", {'message': message, })