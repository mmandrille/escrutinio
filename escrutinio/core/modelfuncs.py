#import Standards
from django.db import models
from django.apps import apps
from django.core.cache import cache
#Import Personales

#Definiciones
def choice_fields(clase):
    def generar_model_fields(clase, lista=[], tail='', submodels=[]):
        submodels.append(clase._meta.model_name)
        for item in dir(clase):#Recorremos todos los elementos de la clase
            atributo = getattr(clase, item)
            if 'field' in dir(atributo):
                if isinstance(atributo.field, models.fields.Field):#en caso de que sea un field
                    if isinstance(atributo, models.fields.related_descriptors.ForwardManyToOneDescriptor):#Si es foreingkey
                        if not atributo.field.related_model in submodels:
                            generar_model_fields(atributo.field.related_model, lista=lista, tail=tail+clase._meta.model_name+'.')
                    elif isinstance(atributo, models.fields.related_descriptors.ManyToManyDescriptor):#Si es un manytomany
                        lista.append((len(lista), tail + clase._meta.model_name +'.'+ item))
                    elif isinstance(atributo, models.fields.related_descriptors.ReverseManyToOneDescriptor):#Related names
                        lista.append((len(lista), tail + clase._meta.model_name +'.'+ item))
            if 'field_name' in dir(atributo) and isinstance(atributo, models.query_utils.DeferredAttribute):#Si es un campo comun!
                lista.append((len(lista), tail + clase._meta.model_name +'.'+ item))
        return lista
    #Realizamos el llamado
    lista = cache.get("show_field-"+clase._meta.model_name)
    if lista is None:
        lista = generar_model_fields(clase)
        cache.set("show_field-"+clase._meta.model_name, lista, 10 * 60)  # guardar la data por 10 minutos, y después sola expira
    return lista