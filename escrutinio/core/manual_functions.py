#ESTA APLICACION ES SOLO PARA PRUEBAS
#Sistema de carga aleatoria de votos para pruebas

#Import Python
from random import randrange
#Import Django

#Import del Proyecto
from .models import Cargo, Total, Lista, Municipio, Mesa, Voto, Conteo

#Definimos funciones
def poblar_votos_test(eleccion, mesa_min, mesa_max):
    print(PARAEJECUTARELIMINARESTALINEA)
    cargos = Cargo.objects.filter(eleccion=eleccion)
    totales = Total.objects.filter(eleccion=eleccion, totales=False)
    totalvotos = Total.objects.get(eleccion=eleccion, totales=True)
    for municipio in Municipio.objects.filter(seccion__provincia__eleccion=eleccion):
        mesas = Mesa.objects.filter(escuela__circuito__municipio=municipio, numero__range=(mesa_min, mesa_max))
        listas = Lista.objects.filter(municipio=municipio)
        for mesa in mesas:
            print("Generando mesa: " + str(mesa.numero))
            Voto.objects.filter(mesa=mesa).delete()
            Conteo.objects.filter(mesa=mesa).delete()
            votos=[]
            conteos=[]
            
            for lista in listas:#Generamos todos los votos validos
                cant = randrange(0,15)
                if lista.agrupacion.alpha_id == 502:
                    cant+= randrange(15,70)
                if lista.agrupacion.alpha_id == 505:
                    cant+= randrange(7,35)
                if lista.agrupacion.alpha_id == 503:
                    cant+= randrange(3,20)
                if lista.agrupacion.alpha_id == 501:
                    cant+= randrange(0,15)            
                for cargo in lista.cargos.all():
                    votos.append(Voto(mesa=mesa, lista=lista, cargo=cargo, cantidad=cant))
            for total in totales:#Generamos valores de totales
                cant = randrange(1,20)
                for cargo in cargos:
                    conteos.append(Conteo(mesa=mesa, total=total, cargo=cargo, cantidad=cant))
            for cargo in cargos:#Generamos la lista de TOTAL DE VOTOS
                stotal=0
                for voto in votos:
                    if cargo == voto.cargo:
                        stotal+= voto.cantidad
                for conteo in conteos:
                    if cargo == conteo.cargo:
                        stotal+= conteo.cantidad
                conteos.append(Conteo(mesa=mesa, total=totalvotos, cargo=cargo, cantidad=stotal))
        Voto.objects.bulk_create(votos)
        Conteo.objects.bulk_create(conteos)
