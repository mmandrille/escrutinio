#Import Python
import logging
#Import Django
from django.core.cache import cache
from django.db.models import Sum, Count
#Import Personales
from .models import Lista#Parametrizacion
from .models import Columna, Cargo, Total#Parametrizaciones
from .models import Municipio#Geograficos
from .models import Mesa, Acta, Voto, Conteo#Votacion
from .models import Voto_Resumen, Conteo_Resumen#Para actualizar resultados

#Definimos funciones utiles
#Trabajo con las tablas
def generar_columnas(id_eleccion, tipo_col, tipo_cargo, extranjeros=False):
    """
        Genera todas las columa que componen un acta(Partidos + Cargos)
        Tiene en cuenta si la mesa es extranjera o no, y si es provincial o para un municipio
        Para evitar hits a la db realiza una cache a la cual consulta
        en caso de futuras actas del mismo municipio
    """
    header = cache.get("header-e:"+str(id_eleccion)+'-tcol:'+str(tipo_col)+'-tcargo:'+str(tipo_cargo)+'ext:'+str(extranjeros))
    if header is None:
        columnas = Columna.objects.filter(eleccion=id_eleccion, tipo=tipo_col).order_by("orden")
        cargos = Cargo.objects.filter(eleccion=id_eleccion, tipo__lte=tipo_cargo).order_by("orden")
        if extranjeros:
            cargos = cargos.filter(tipo= 3)
        header = [c for c in  columnas]+[c for c in cargos]
        cache.set("header-e:"+str(id_eleccion)+'-tcol:'+str(tipo_col)+'-tcargo:'+str(tipo_cargo)+'ext:'+str(extranjeros), header)
    return header

def generar_cargos(eleccion, tipo_cargo, extranjeros=False):
    """
        Genera el listado de cargos(gobernador, diputados, etc)
        Tiene en cuenta si la mesa es extranjera o no, y si es provincial o para un municipio
        Para evitar hits a la db realiza una cache a la cual consulta
        en caso de futuras actas del mismo municipio
    """
    dict_cargos = cache.get("dict_cargos-eleccion:"+str(eleccion.id)+'-tcargo:'+str(tipo_cargo)+'ext:'+str(extranjeros))
    if dict_cargos is None:
        cargos = Cargo.objects.filter(eleccion=eleccion, tipo__lte=tipo_cargo)
        if extranjeros:
            cargos = cargos.filter(tipo= 3)
        dict_cargos = {}
        for cargo in cargos:
            dict_cargos[cargo.id]= cargo
        cache.set("dict_cargos-eleccion:"+str(eleccion.id)+'-tcargo:'+str(tipo_cargo)+'ext:'+str(extranjeros), dict_cargos)
    return dict_cargos

def generar_totales(eleccion):
    """
        Genera el listado de totales(votos blancos, nulos, etc)
        Para evitar hits a la db realiza una cache a la cual consulta
        en caso de futuras actas del mismo municipio
    """
    dict_totales = cache.get("dict_totales-eleccion:"+str(eleccion.id))
    if dict_totales is None:
        totales = Total.objects.filter(eleccion=eleccion)
        dict_totales = {}
        for total in totales:
            dict_totales[total.id]= total
        cache.set("dict_totales-eleccion:"+str(eleccion.id), dict_totales)
    return dict_totales

def generar_listas(mesa):
    """
        Entrega las listas para una mesa
        Para evitar hits a la db realiza una cache a la cual consulta
        en caso de futuras actas del mismo municipio
    """
    dict_listas = cache.get("dict_listas-municipio:"+str(mesa.escuela.circuito.municipio.id))
    if dict_listas is None:
        listas = Lista.objects.select_related('agrupacion').prefetch_related('cargos').filter(municipio=mesa.escuela.circuito.municipio.id).order_by('agrupacion__alpha_id', 'orden')
        dict_listas = {}
        for lista in listas:
            dict_listas[lista.id]= lista
        cache.set("dict_listas-municipio:"+str(mesa.escuela.circuito.municipio.id), dict_listas)
    return dict_listas

#Funciones Genericas
def obtener_cvotantes(eleccion, municipio=None):
    """
        Retorna la Cantidad de Votantes
        Puede ser para un municipio especifico o para toda la provincia
    """
    cant = 0
    actas = Acta.objects.filter(mesa__escuela__circuito__municipio__seccion__provincia__eleccion=eleccion)
    if municipio is not None:
        actas = actas.filter(mesa__escuela__circuito__municipio=municipio)
    cant+= actas.aggregate(sum=Sum('celectores'))['sum']
    return cant

def porcentaje_escrutadas(eleccion):
    """
        Devuelve un array con   key: id_municipio
                                value: porcentaje de mesas escrutadas del municipio
    """
    porcentajes = {}
    municipios = Municipio.objects.filter(seccion__provincia__eleccion=eleccion).annotate(cmesas=Count('circuitos__escuelas__mesas'))
    for m in municipios:
        porcentajes[m.id] = 0
 
    votos = Voto.objects.select_related('mesa__escuela__circuito__municipio')\
                        .filter(mesa__escuela__circuito__municipio__in=municipios)\
                        .values('mesa__numero', 'mesa__escuela__circuito__municipio').distinct()
    for v in votos:
        porcentajes[v['mesa__escuela__circuito__municipio']] += 1

    for municipio in municipios:
        cant_mesas = municipio.cmesas
        cant_cargadas = porcentajes[municipio.id]
        try:
            porc = (100.00 / cant_mesas) * cant_cargadas
        except:
            porc = 0.0
        porcentajes[municipio.id] = round(porc,5)#Limitamos para tener control en actualizar resultados
    return porcentajes

def replicar_listas(municipio_origen, municipio_destino):
    """
        Luego de cargar un municipio esta funcion sirve para evitar parametrizar todas las
        listas en otro, copia las existentes al municipio vacio
        Luego se deben agregar y eliminar las que no correspondan + configurar cargos
    """
    listas = Lista.objects.filter(municipio=municipio_origen)#Obtenemos las que vamos a usar
    Lista.objects.filter(municipio=municipio_destino).delete()#Eliminamos las del municipio destino
    for lista in listas:
        lista.pk = None
        lista.municipio = municipio_destino
        lista.save()
    return True

#RESULTADOS
def actualizar_resultados(eleccion):
    """ 
        Esta funcion genera un totalizado de resultados por municipio
        cada vez que se detecta que en el municipio se ingresaron mesas nuevas
        de esta forma evitamos procesamiento innecesario a la hora de volver a mostrar
        nuevamente los mismos resultados
        PERO no detecta cambios en caso de que solo se toquen actas ya cargadas
    """
    #Obtenemos elementos basicos:
    porcentajes = porcentaje_escrutadas(eleccion)
    municipios = Municipio.objects.filter(seccion__provincia__eleccion=eleccion)
    #Controlamos si estan actualizados:
    for m in municipios:
        votos_r = Voto_Resumen.objects.filter(municipio=m)
        conteos_r = Conteo_Resumen.objects.filter(municipio=m)
        porc_muni = porcentajes[m.id]
        if porc_muni:
            if votos_r:
                vr = votos_r.first()
                if vr.pescrutado != porc_muni:
                    votos_r.delete()
                    conteos_r.delete()
        if not votos_r and porc_muni:
            #Si los eliminamos por que estaban desactualizados:
            votos_r, conteos_r = [], []#Preparamos listas para bulk_create
            #Actualizamos Votos
            new_votos_resumen = {}
            votos = Voto.objects.filter(mesa__escuela__circuito__municipio=m).select_related('lista', 'cargo')
            for v in votos:
                if (v.lista, v.cargo) in new_votos_resumen:
                    new_votos_resumen[(v.lista, v.cargo)]+= v.cantidad
                else:
                    new_votos_resumen[(v.lista, v.cargo)]= v.cantidad
            for nvr in new_votos_resumen:
                votos_r.append(Voto_Resumen(municipio=m, pescrutado=porc_muni, lista=nvr[0] , cargo=nvr[1], cantidad=new_votos_resumen[nvr]))
            #Actualizamos Conteos
            new_conteos_resumen = {}
            conteos = Conteo.objects.filter(mesa__escuela__circuito__municipio=m).select_related('total', 'cargo')
            for c in conteos:
                if (c.total, c.cargo) in new_conteos_resumen: 
                    new_conteos_resumen[(c.total, c.cargo)]+= c.cantidad
                else: 
                    new_conteos_resumen[(c.total, c.cargo)] = c.cantidad
            for cnr in new_conteos_resumen:
                conteos_r.append(Conteo_Resumen(municipio=m, pescrutado=porc_muni, total=cnr[0] , cargo=cnr[1], cantidad=new_conteos_resumen[cnr]))
            Voto_Resumen.objects.bulk_create(votos_r)
            Conteo_Resumen.objects.bulk_create(conteos_r)

def proyectar(eleccion, votos, municipio=None, conteos=None):
    """
        Genera una proyeeccion sobre los votos y conteos 
        teniendo en cuenta el % escrutado para cada municipio
        A partir de la cantidad de mesas escrutadas
        Para toda la provincia o sobre un municipio especifico
    """
    porcentajes = porcentaje_escrutadas(eleccion)
    if votos:
        for v in votos:
            if v.cantidad:
                v.cantidad = int(100.0 / (porcentajes[v.municipio.id] / v.cantidad))
    if conteos:
        for c in conteos:
            if c.cantidad:
                c.cantidad = c.cantidad = int(100.0 / (porcentajes[c.municipio.id] / c.cantidad))
    return votos, conteos#, muni_sin_calcular

def proyectar_bruto(eleccion, votos, municipio=None, conteos=None):
    """
        Genera una proyeeccion sobre los votos y conteos
        A partir de la cantidad de mesas escrutadas en toda la provincia o sobre un municipio especifico
    """
    #Obtenemos un porcentaje general
    if not municipio:
        cmesas = Mesa.objects.filter(escuela__circuito__municipio__seccion__provincia__eleccion=eleccion).count()
        cescrutadas = Voto.objects.filter(mesa__escuela__circuito__municipio__seccion__provincia__eleccion=eleccion).values('mesa__numero').distinct().count()
    else:
        cmesas = Mesa.objects.filter(escuela__circuito__municipio=municipio).count()
        cescrutadas = Voto.objects.filter(mesa__escuela__circuito__municipio=municipio).values('mesa__numero').distinct().count()
    
    porcentaje_general = ( 100.0 / cmesas ) * cescrutadas

    if votos:
        for v in votos:
            if v.cantidad:
                v.cantidad = int(100.0 / (porcentaje_general / v.cantidad))
    if conteos:
        for c in conteos:
            if c.cantidad:
                c.cantidad = c.cantidad = int(100.0 / (porcentaje_general / c.cantidad))
    return votos, conteos