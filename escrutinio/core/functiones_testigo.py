#imports python

#imports django

#imports personales
from core.models import Mesa, Votante
from .generic_classes import Actualizacion

#Definimos funciones
def marcar_mesas_testigo(eleccion, provincia, lineas):
    Mesa.objects.filter(escuela__circuito__municipio__seccion__provincia=provincia).update(testigo=False)
    for linea in lineas[1:]:
        linea = linea.split(',')
        if linea[0]:
            mesa = Mesa.objects.get(numero=linea[0])
            mesa.testigo = True
            mesa.puntaje = linea[1]
            mesa.save()

def actualizar_testigos(padron_viejo):
    """
        Para que esta funcion realize su trabajo se debe tener cargado el padron ACTUAL
        Y EL PERTENECIENTE A LA ELECCION DE LA QUE SE IMPORTAN LAS MESAS TESTIGO
    """
    def checkear_calidad(act, votantes_viejos, mesa_propuesta):
        act.total_votantes = 0
        act.calidad = 0
        votantes_nuevos = [ v.num_doc for v in mesa_propuesta.votantes.all().filter(padron__actual=True)]
        for votante_viejo in votantes_viejos:#Recorremos todos los votantes de la mesa vieja
            act.total_votantes += 1
            if votante_viejo.num_doc in votantes_nuevos:
                act.calidad += 1
        return (100 / act.total_votantes) * act.calidad

    actualizaciones = []
    dict_mesas_viejas = {m.numero: Votante.objects.filter(mesa=m, padron=padron_viejo) for m in Mesa.objects.filter(testigo=True)}
    for num_mesa_vieja in dict_mesas_viejas:#recorremos las mesas viejas
        act = Actualizacion()
        act.vieja = num_mesa_vieja
        votantes_viejos = dict_mesas_viejas[num_mesa_vieja]
        votantes_nuevos = Votante.objects.filter(padron__actual=True,
                                                 num_doc__in=votantes_viejos.values('num_doc'))

        print("Intentamos encontrar la mejor mesa para: " + str(num_mesa_vieja))

        for votante in votantes_nuevos:
            if act.nueva != votante.mesa:#Evitamos analizar LA MISMA mesa
                calidad = checkear_calidad(act, votantes_viejos, votante.mesa)
                if calidad > 65:
                    act.porc_calidad = calidad
                    act.nueva = votante.mesa
                    print("Analizada: " + str(votante.mesa.numero) + ' Dio una calidad de: ' + str(calidad))
                    break
                elif calidad > act.porc_calidad:
                    act.porc_calidad = calidad
                    act.nueva = votante.mesa
                    print('Aceptamos por ahora la opcion:' + str(votante.mesa.numero) + 'con calidad: ' + str(calidad))
        mesa_vieja = Mesa.objects.get(escuela__circuito__municipio__seccion__provincia=padron_viejo.provincia, numero=num_mesa_vieja)
        mesa_vieja.testigo = False
        mesa_vieja.save()
        act.nueva.testigo = True
        act.nueva.save()
        print("-------------------------------------")
        actualizaciones.append(act)
    return actualizaciones