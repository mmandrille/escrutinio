from django.conf.urls import url
from django.urls import path
#Import personales
from . import views, tasks

app_name = 'core'
urlpatterns = [
    #Basicas:
    url(r'^$', views.home, name='home'),

    path('contacto/', views.contacto, name='contacto'),
    path('donde_voto/', views.donde_voto, name='donde_voto'),
    #Cargas individuales
    path('cargar/<int:eleccion_id>/', views.select_acta, name='select_acta'),
    path('cargar/<int:eleccion_id>/<int:mesa_num>/', views.cargar_acta, name='cargar_acta'),
    path('subir_acta/<int:eleccion_id>/<int:mesa_num>/', views.cargar_scan, name='cargar_scan'),
    #Resultados
    path('resultados/<int:eleccion_id>/', views.resultados, name='resultados'),
    path('resultados/<int:eleccion_id>/<int:municipio_id>/', views.resultados, name='resultados'),
    path('resultados/<int:eleccion_id>/<int:municipio_id>/<int:circuito_id>', views.resultados, name='resultados'),
    #Resultados Proyectados
    path('proyectado/<int:eleccion_id>/', views.resultados, name='resultados'),
    path('proyectado/<int:eleccion_id>/<int:municipio_id>/', views.resultados, name='resultados'),
    #Resultados Proyectados Brutos
    path('proyectadobruto/<int:eleccion_id>/', views.resultados, name='resultados'),
    path('proyectadobruto/<int:eleccion_id>/<int:municipio_id>/', views.resultados, name='resultados'),
    #ranking
    path('ranking/<int:eleccion_id>/<int:cargo_id>/', views.ranking, name='ranking'),
    path('ranking/<int:eleccion_id>/<int:cargo_id>/<int:municipio_id>/', views.ranking, name='ranking'),
    #ranking proyectados
    path('rankpro/<int:eleccion_id>/<int:cargo_id>/', views.ranking, name='ranking'),
    path('rankpro/<int:eleccion_id>/<int:cargo_id>/<int:municipio_id>/', views.ranking, name='ranking'),
    #ranking proyectados Bruto
    path('rankprobruto/<int:eleccion_id>/<int:cargo_id>/', views.ranking, name='ranking'),
    path('rankprobruto/<int:eleccion_id>/<int:cargo_id>/<int:municipio_id>/', views.ranking, name='ranking'),
    #Graficos
    path('grafico/<str:cache_name>/', views.grafico, name='grafico'),
    path('grafico/<int:eleccion_id>/<int:cargo_id>/<int:municipio_id>/', views.grafico, name='grafico'),
    #Mesas Cargadas
    path('cargadas/<int:eleccion_id>/', views.mesas_cargadas, name='mesas_cargadas'),
    path('cargadas/<int:eleccion_id>/<int:municipio_id>/', views.mesas_cargadas, name='mesas_cargadas'),
    #Stat de mesas
    path('situacion_escrutinio/<int:eleccion_id>/', views.situacion_escrutinio, name='situacion_escrutinio'),
    path('situacion_escrutinio/<int:eleccion_id>/<int:municipio_id>/', views.situacion_escrutinio, name='situacion_escrutinio'),
    path('listado_mesas_csv/<int:eleccion_id>/', views.listado_mesas_csv, name='listado_mesas_csv'),
    #replicar actas
    path('replicar_actas/<int:eleccion_id>/', views.replicar_actas, name='replicar_actas'),
    path('replicar_eleccion/', views.replicar_eleccion, name='replicar_eleccion'),
    path('download_oficial/', views.download_oficiales, name='download_oficiales'),
    #Responsables
    path('cargar_resp/<int:eleccion_id>/', views.cargarresponsable, name='cargarresponsable'),
    path('responsables/<int:eleccion_id>/', views.verresponsables, name='verresponsables'),
    path('responsables/<int:eleccion_id>/<int:municipio_id>/', views.verresponsables, name='verresponsables'),
    #Controles
    path('controles/', views.controles, name='controles'),
    path('cdiferencial/', views.control_diferencial, name='control_diferencial'),
    path('cestadistico/', views.control_estadistico, name='control_estadistico'),
    path('ctotales/', views.control_nopositivos, name='control_nopositivos'),
    path('climites/', views.control_limites, name='control_limites'),
    
    #carga masiva
    path('rellenar_totales/<int:eleccion_id>/', views.rellenar_totales, name='rellenar_totales'),
    path('uploadcsv/', views.upload_csv, name='upload_csv'),
    path('uploadcsv/<str:tipo>/', views.upload_csv, name='upload_csv'),
    #Task Manager:
    path('task_progress/<str:queue_name>', tasks.task_progress, name='task_progress'),
]