#import python
import copy
#Imports Django
from django.shortcuts import render
#Import Particulares
from zeep import Client
#Task Manager Models
from background_task import background
from background_task.models import Task as bg_Tasks
from background_task.models_completed import CompletedTask as bg_CompletedTask


#Import Propios
from .models import Eleccion, Progress_Links, Municipio, Mesa, Voto, Cargo, Total, Conteo

#Rutinas Base:
def crear_progress_link(new_queue):
    print("Se creo link a: " + new_queue)
    p = Progress_Links()
    p.tarea = new_queue
    p.progress_url = '/inscribir/task_progress/' + new_queue
    p.save()
    return new_queue

def task_progress(request, queue_name):
    tareas_en_progreso = bg_Tasks.objects.filter(queue= queue_name)
    tareas_terminadas = bg_CompletedTask.objects.filter(queue= queue_name)
    return render(request, 'utils/task_progress.html', {'tarea_queue': queue_name, 
                                                'tareas_totales': (len(tareas_en_progreso)+len(tareas_terminadas)) ,
                                                'tareas_en_cola': len(tareas_en_progreso), 
                                                'tareas_terminadas': len(tareas_terminadas),
                                                "refresh": True })

#Tareas
@background(schedule=60)
def download_mesas(eleccion_id, link, usuario, password, mesa_inicio, mesa_fin):
    eleccion = Eleccion.objects.get(pk=eleccion_id)
    client = Client(link)
    municipios = { m.alpha_id : m for m in Municipio.objects.prefetch_related('listas').filter(seccion__provincia__eleccion=eleccion) }
    dict_listas = { m.alpha_id : { l.agrupacion.alpha_id : l for l in m.listas.all()} for m in municipios.values() }
    dict_mesas = { m.numero : m for m in Mesa.objects.filter(escuela__circuito__municipio__seccion__provincia__eleccion=eleccion, numero__range=(mesa_inicio,mesa_fin)) }
    dict_cargadas = [ v['mesa__numero'] for v in Voto.objects.filter(mesa__escuela__circuito__municipio__seccion__provincia__eleccion=eleccion).values('mesa__numero').distinct()]
    dict_cargos = { c.orden : c for c in Cargo.objects.filter(eleccion=eleccion)}
    dict_totales = {9990: Total.objects.get(eleccion=eleccion, orden=1),
                    9991: Total.objects.get(eleccion=eleccion, orden=2),
                    9992: Total.objects.get(eleccion=eleccion, orden=3),
                    9993: Total.objects.get(eleccion=eleccion, orden=4),}
    cant = 0
    for m in dict_mesas:
        if m not in dict_cargadas:
            print("Descargando mesa: " + str(m))
            cant += 1
            votos, conteos = [], []
            dict_votos = client.service.Execute(Usu=usuario, Pas=password, Mesa=m)['Datwsmesa']['SDTDatWsMesa']
            for dvoto in dict_votos:
                if dvoto['ListaId'] < 9000:
                    item = Voto()
                    item.lista = dict_listas[dvoto['MunicipioId']][dvoto['ListaId']]
                    items = votos
                else:
                    item = Conteo()
                    item.total = dict_totales[dvoto['ListaId']]
                    items = conteos
                item.mesa = dict_mesas[m]
                if dvoto['VGobernador'] != '--' and int(dvoto['VGobernador']) > 0:
                    item.cargo = dict_cargos[1]
                    item.cantidad = int(dvoto['VGobernador'])
                    items.append(copy.copy(item))
                if dvoto['VDipProv'] != '--' and int(dvoto['VDipProv']) > 0:
                    item.cargo = dict_cargos[2]
                    item.cantidad = int(dvoto['VDipProv'])
                    items.append(copy.copy(item))
                if dvoto['VIntendente'] != '--' and int(dvoto['VIntendente']) > 0:
                    item.cargo = dict_cargos[3]
                    item.cantidad = int(dvoto['VIntendente'])
                    items.append(copy.copy(item))
                if dvoto['VComMuni'] != '--' and int(dvoto['VComMuni']) > 0:
                    item.cargo = dict_cargos[3]
                    item.cantidad = int(dvoto['VComMuni'])
                    items.append(copy.copy(item))
                if dvoto['VConcejal'] != '--' and int(dvoto['VConcejal']) > 0:
                    item.cargo = dict_cargos[4]
                    item.cantidad = int(dvoto['VConcejal'])
                    items.append(copy.copy(item))
            if votos and conteos:
                Voto.objects.bulk_create(votos)#Si se ingresaron votos, los mandamos masivamente
                Conteo.objects.bulk_create(conteos)#Si se ingresaron conteos, los mandamos masivamente
    return cant