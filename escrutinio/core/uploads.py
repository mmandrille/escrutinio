#Import Standards

#Import Personales
from .models import Lista#Parametrizacion
from .models import Cargo, Total#Parametrizaciones
from .models import Seccion, Municipio, Circuito#Geograficos
from .models import Escuela, Mesa, Acta, Voto, Conteo#Votacion
from .models import Padron, Votante#Padrones

#Upload masivos:
def cargar_geografico(provincia, lineas):
    cant = 0
    for linea in lineas:
        cant += 1
        if cant % 15 == 0:
            print("Se han cargado: " + str(cant) + ' circuitos.')
        linea=linea.split(',')
        if linea[0]:
            seccion = Seccion.objects.get_or_create(
                provincia= provincia,
                alpha_id= linea[0],
                nombre= linea[1])[0]
            municipio = Municipio.objects.get_or_create(
                seccion= seccion,
                alpha_id= linea[2],
                nombre= linea[3])[0]
            circuito = Circuito.objects.get_or_create(
                municipio= municipio,
                alpha_id= linea[4],
                nombre= linea[5])[0]
    print("Circuitos Totales: " + str(cant))

def cargar_mesas(provincia, lineas):
    cant = 0
    dict_circuito = {c.alpha_id : c for c in Circuito.objects.filter(municipio__seccion__provincia=provincia)}
    for linea in lineas:
        cant+= 1
        if cant % 100 == 0:
            print("Se han cargado: " + str(cant) + ' mesas.')
        linea=linea.split(',')
        if linea[0]:
            circuito = dict_circuito[linea[0]]
            escuela = Escuela.objects.get_or_create(
                circuito= circuito,
                nombre= linea[1],
                direccion= linea[2])[0]
            mesa = Mesa.objects.get_or_create(
                escuela= escuela,
                numero= linea[3])[0]
            acta = Acta.objects.get_or_create(
                mesa = mesa,
                celectores = linea[4])[0]
    print("Mesas Totales: " + str(cant))

def cargar_padron(provincia, padron_fecha, descripcion, lineas):
    #Desactivamos los padrones mas viejos
    Padron.objects.filter(provincia=provincia, fecha__lt=padron_fecha).update(actual=False)
    #Creamos el nuevo padron
    padron = Padron.objects.get_or_create(
                provincia= provincia,
                descripcion= descripcion,
                fecha= padron_fecha)[0]
    mesas = Mesa.objects.filter(escuela__circuito__municipio__seccion__provincia=provincia).order_by('numero')
    dict_mesas = {m.numero: m for m in mesas}
    #Eliminamos todos los votantes que tenia este padron previamente
    Votante.objects.filter(padron=padron).delete()
    votantes = []
    #Procesamos todas las lineas del padron
    cant = 0
    for linea in lineas:
        cant+= 1
        if cant % 10000 == 0:
            print("Se han cargado: " + str(cant) + ' Votantes.')
        linea=linea.split(';')
        if linea[0] and linea[0] != 'TIPO_DOC':
            try:
                votantes.append(Votante(padron=padron,
                                        tipo_doc = linea[0],
                                        mesa = dict_mesas[int(linea[6])],
                                        num_doc = int(linea[1]),
                                        apellido = linea[2],
                                        nombre = linea[3],
                                        clase = int(linea[4]),
                                        domicilio = linea[5],
                                        genero = linea[7]))
            except:#A veces no encuentra la mesa si es un padron viejo.
                pass
    Votante.objects.bulk_create(votantes)
    print("Votantes Totales: " + str(cant))

def cargar_votos(eleccion, municipio, lineas):
    mesas = {m.numero: m for m in Mesa.objects.filter(escuela__circuito__municipio=municipio)}
    cargos = {c.orden: c for c in Cargo.objects.filter(eleccion=eleccion)}
    listas = {l.agrupacion.alpha_id: l for l in Lista.objects.filter(agrupacion__eleccion=eleccion, municipio=municipio)}
    dict_conv_conteos = {1: 900, 2: 901, 3:902, 4:  903, 9:  904}
    totales = {dict_conv_conteos[t.orden]: t for t in Total.objects.filter(eleccion=eleccion)}
    votos = []
    conteos = []
    mesas_cargadas = []
    cant = 0
    for linea in lineas:
        cant += 1
        if cant % 1000 == 0:
            print("Se han procesado: " + str(cant) + ' lineas.')
        linea = linea.replace('"', '').replace('\r', '')
        linea=linea.split(',')
        if linea != [''] and int(linea[0]) in mesas:
            mesas_cargadas.append(int(linea[0]))
            if int(linea[1]) < 900:
                votos.append(Voto(mesa=mesas[int(linea[0])],
                                    lista=listas[int(linea[1])],
                                    cargo=cargos[int(linea[2])],
                                    cantidad=int(linea[3]),
                                    ))
            else:
                conteos.append(Conteo(mesa=mesas[int(linea[0])],
                                        total=totales[int(linea[1])],
                                        cargo=cargos[int(linea[2])],
                                        cantidad=int(linea[3]),
                                            ))
    if Voto.objects.filter(mesa__escuela__circuito__municipio=municipio).count() < len(votos):
        Voto.objects.filter(mesa_id__in=mesas_cargadas).delete()
        Conteo.objects.filter(mesa_id__in=mesas_cargadas).delete()
        Voto.objects.bulk_create(votos)
        Conteo.objects.bulk_create(conteos)
    print("Votantes Totales: " + len(votos))

