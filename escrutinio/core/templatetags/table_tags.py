#Import Standards
from django import template
from django.core.cache import cache
#Registramos el template
register = template.Library()
#Imports del proyecto
from core.models import Cargo
from core.modelfuncs import choice_fields


#Definimos nuestros custom tags
@register.simple_tag
def obtener_cargos(mesa):
    cargos = {}
    for lista in mesa.escuela.circuito.municipio.listas.all():
        for cargo in lista.cargos.all():
            cargos[cargo.orden] = cargo
    return [c for c in cargos.values()]

@register.simple_tag
def obtener_valor(objeto, field):
    valor = objeto
    for c in choice_fields(valor._meta.model):
            if c[0] == field:
                    field= c[1]
    for f in field.split('.')[1:]:
            valor = getattr(valor, f)
    return valor

@register.simple_tag
def obtener_votos(votos, lista, cargo, agrupacion=None):
    cant=0
    if agrupacion:
        for v in votos:
            if v.lista.agrupacion==agrupacion and v.cargo==cargo:
                cant+= v.cantidad
    else:
        for v in votos:
            if v.lista==lista and v.cargo==cargo:
                cant+= v.cantidad
    return cant

@register.simple_tag
def obtener_pvotos(votos, lista, cargo, agrupacion=None):
    cant, total= 0, 0
    if agrupacion:
        for v in votos:
            if v.cargo == cargo:
                total+= v.cantidad
                if v.lista.agrupacion == agrupacion:
                    cant+= v.cantidad
    else:
        for v in votos:
            if v.cargo == cargo:
                total+= v.cantidad
                if v.lista==lista:
                    cant+= v.cantidad
    if total and cant:
        return str(round(100.0 / total * cant, 2)) + '%'
    else:
        return '0%'

@register.simple_tag
def obtener_conteos(conteos, total, cargo):
    cant=0
    for c in conteos:
        if c.total==total and c.cargo==cargo:
            cant+= c.cantidad
    return cant

@register.simple_tag
def obtener_pconteos(conteos, total, cargo):
    cant, ctotal= 0, 0
    for c in conteos:
        if c.total==total and c.cargo==cargo:
            cant+= c.cantidad
        if c.cargo==cargo and c.total.totales:
            ctotal+= c.cantidad
    if ctotal and cant:
        return str(round((100.0 / ctotal) * cant, 2)) + '%'
    else:
        return '0%'

@register.simple_tag
def control_totales(columnas, votos, conteos):
    for columna in columnas:
        if isinstance(columna, Cargo):
            cantidad = (sum([voto.cantidad for voto in votos if voto.cargo == columna]))
            cantidad+= (sum([conteo.cantidad for conteo in conteos if conteo.cargo == columna and not conteo.total.totales]))
            correcto = sum([conteo.cantidad for conteo in conteos if conteo.cargo == columna and conteo.total.totales]) == cantidad
            if not correcto:
                break
    if correcto:
        return " total_correcto"
    else:
        return " total_incorrecto"