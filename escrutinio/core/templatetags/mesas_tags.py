#Import Standards
from django import template
#Registramos el template
register = template.Library()

#Import del proyecto
from core.models import Mesa, Voto, Votante

#Definimos nuestros custom tags
@register.simple_tag
def stat_mesas(eleccion, municipio=None, circuito=None):
    mesas = Mesa.objects.select_related('escuela__circuito__municipio').filter(escuela__circuito__municipio__seccion__provincia__eleccion=eleccion)
    votos = Voto.objects.select_related('mesa__escuela__circuito__municipio').filter(mesa__in=mesas).values('mesa__numero', 'mesa__escuela').distinct()
    votantes = Votante.objects.filter(padron__actual=True)
    if municipio is not None:
        mesas = mesas.filter(escuela__circuito__municipio=municipio)
        votos = votos.filter(mesa__escuela__circuito__municipio=municipio)
        votantes = votantes.filter(mesa__escuela__circuito__municipio=municipio)
    if circuito is not None:
        mesas = mesas.filter(escuela__circuito=circuito)
        votos = votos.filter(mesa__escuela__circuito=circuito)
        votantes = votantes.filter(mesa__escuela__circuito=circuito)

    cant_mesas = mesas.count()
    cant_cargadas = votos.count()
    total_votantes =  votantes.count()
    try:
        porcentaje = (100.00 / cant_mesas) * cant_cargadas
    except:
        porcentaje = 0.0
        print("Se deben cargar mesas a la eleccion y municipio")
    return [cant_mesas, cant_cargadas, porcentaje, total_votantes]