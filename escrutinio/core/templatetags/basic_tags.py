#Import oficial
from django import template

#Import Personales
register = template.Library() 

#Definimos nuestros customtags
@register.simple_tag
def pertenece(user, group_name):
    return user.groups.filter(name=group_name).exists() 