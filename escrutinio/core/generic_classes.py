#Imports del modulo
from .models import Mesa, Circuito

#Definimos clases genericas
class Charts():
    titulo = "Resultado"
    table = []#[['Partido','Cantidad']] No lo puedo poner si no es imposible ordenarla
    cache_name = 'chart'
    width = 550
    height = 400

class Items():
    circuito = Circuito()
    nombre = ''
    cmesas = 0
    cescrutadas = 0
    porcentaje = ''
    cvotantes = 0

class Actualizacion:
    vieja = 0
    nueva = Mesa()
    calidad = 0
    porc_calidad = 0
    total_votantes = 0
    def __str__(self):
        return(str(self.nueva.numero) + '- Calidad' + str(self.porc_calidad) + '%')