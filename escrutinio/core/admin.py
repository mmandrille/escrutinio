#Modulos Standard
from django.contrib import admin

#Modulos para no permitir escalacion de privilegios
from django.contrib.auth.admin import UserAdmin, User

#Incluimos modelos
from .models import Eleccion, Cargo, CantCargos, Columna, Total
from .models import Provincia, Seccion, Municipio, Circuito, Escuela
from .models import Mesa, Acta
from .models import Agrupacion, Lista
from .models import Voto, Denuncia, Responsable
from .models import Padron, Votante
from .modelforms import ColumnaForm

#Modificacion del panel de administrador para no permitir escalacion de privilegios
class RestrictedUserAdmin(UserAdmin):
    model = User
    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(RestrictedUserAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        user = kwargs['request'].user
        if not user.is_superuser:
            if db_field.name == 'groups':
                field.queryset = field.queryset.filter(id__in=[i.id for i in user.groups.all()])
            if db_field.name == 'user_permissions':
                field.queryset = field.queryset.filter(id__in=[i.id for i in user.user_permissions.all()])
            if db_field.name == 'is_superuser':
                field.widget.attrs['disabled'] = True
        return field

#Realizamos modificaciones sobre los modelos
class CantCargosInline(admin.TabularInline):
     model = CantCargos
     ordering = ['municipio__alpha_id']

class CargoAdmin(admin.ModelAdmin):
    list_filter = ['eleccion', ]
    ordering = ['eleccion', 'orden']
    inlines = [CantCargosInline, ]

class CargoInline(admin.TabularInline):
    model = Cargo
    list_filter = ['eleccion']
    ordering = ['orden']
    def get_extra(self, request, obj=None, **kwargs):
        return 1

class TotalInline(admin.TabularInline):
    model = Total
    list_filter = ['eleccion']
    ordering = ['orden']
    def get_extra(self, request, obj=None, **kwargs):
        return 1

class EleccionAdmin(admin.ModelAdmin):
    inlines = [CargoInline, TotalInline]

class ColumnaAdmin(admin.ModelAdmin):
    list_filter = ['eleccion', 'tipo' ]
    ordering = ['orden']
    def get_form(self, request, obj=None, **kwargs):
        kwargs['form'] = ColumnaForm
        return super().get_form(request, obj, **kwargs)

class TotalAdmin(admin.ModelAdmin):
    list_filter = ['eleccion', ]
    ordering = ['orden']

class SeccionAdmin(admin.ModelAdmin):
    ordering = ['alpha_id']

class MunicipioAdmin(admin.ModelAdmin):
    list_filter = ['seccion__provincia__eleccion', 'seccion']
    ordering = ['alpha_id']

class CircuitoAdmin(admin.ModelAdmin):
    list_filter = ['municipio__seccion__provincia__eleccion', 'municipio', 'municipio__seccion',]
    ordering = ['alpha_id']

class EscuelaAdmin(admin.ModelAdmin):
    list_filter = ['circuito__municipio__seccion__provincia__eleccion', 'circuito', 'circuito__municipio', 'circuito__municipio__seccion',]
    search_fields = ['nombre']

class ActaInline(admin.TabularInline):
    model = Acta

class MesaAdmin(admin.ModelAdmin):
    ordering = ['numero']
    list_filter = ['escuela__circuito__municipio__seccion__provincia__eleccion', 'testigo', 'escuela__circuito__municipio']
    inlines = [ActaInline]

class ListaInline(admin.TabularInline):
    model = Lista
    def get_extra(self, request, obj=None, **kwargs):
        return 1

class AgrupacionAdmin(admin.ModelAdmin):
    list_filter = ['eleccion']
    ordering = ['alpha_id']
    search_fields = ['nombre']
    inlines = [ListaInline]

class ListaAdmin(admin.ModelAdmin):
    model = Lista
    list_filter = ['agrupacion__eleccion', 'municipio',]
    ordering = ['agrupacion__alpha_id', 'orden']
    search_fields = ['nombre']
    def get_form(self, request, obj=None, **kwargs):
        form = super(ListaAdmin, self).get_form(request, obj, **kwargs)
        if obj and obj.agrupacion.eleccion:
            form.base_fields['cargos'].queryset = Cargo.objects.filter(eleccion=obj.agrupacion.eleccion)
        elif obj is None and 'status' in form.base_fields:
            del form.base_fields['status']
        return form

class DenunciaAdmin(admin.ModelAdmin):
    list_filter = ['municipio', 'escuela']

class ResponsableAdmin(admin.ModelAdmin):
    raw_id_fields = ('mesa',)
    list_filter = ['tipo', 'municipio']
    search_fields = ['nombres', 'apellido', 'municipio__nombre', 'mesa__numero',]

#Registramos modificaciones para no permitir escalacion de privilegios
admin.site.unregister(User)
admin.site.register(User, RestrictedUserAdmin)
# Register your models here.
admin.site.register(Eleccion, EleccionAdmin)
admin.site.register(Cargo, CargoAdmin)
admin.site.register(Columna, ColumnaAdmin)
admin.site.register(Total, TotalAdmin)
admin.site.register(Provincia, )
admin.site.register(Seccion, )
admin.site.register(Municipio, MunicipioAdmin)
admin.site.register(Circuito, CircuitoAdmin)
admin.site.register(Escuela, EscuelaAdmin)
admin.site.register(Mesa, MesaAdmin)
admin.site.register(Agrupacion, AgrupacionAdmin)
admin.site.register(Lista, ListaAdmin)
admin.site.register(Denuncia, DenunciaAdmin)
admin.site.register(Responsable, ResponsableAdmin)
admin.site.register(Padron,)
