#Import Standard
from django import forms
from django.forms import ModelForm
#import extras:

#Import Personales
from .models import Lista, Columna, Responsable, Municipio
from .models import Votante, Mesa, Escuela
from .modelfuncs import choice_fields

#Definiciones del modelo
class ColumnaForm(ModelForm):
    attrs_dict= { 'class': 'required' }
    show_field= forms.ChoiceField(choices=(), widget=forms.Select(attrs=attrs_dict))
    class Meta:
        model= Columna
        fields= '__all__'
    def __init__(self, *args, **kwargs):
        super(ColumnaForm, self).__init__(*args, **kwargs)
        self.fields["show_field"].choices = choice_fields(Lista)#Seria ideal que no este hardcodeado

class ResponsableForm(ModelForm):
    class Meta:
        model= Responsable
        fields= '__all__'
        exclude = ('foto', 'num_doc', 'autorizado')
    def __init__(self, *args, **kwargs):
        eleccion_id = kwargs.pop('eleccion_id', None)
        super(ResponsableForm, self).__init__(*args, **kwargs)
        self.fields['mesa'].queryset = Mesa.objects.filter(escuela__circuito__municipio__seccion__provincia__eleccion__id=eleccion_id).select_related('escuela')
        self.fields['escuela'].queryset = Escuela.objects.filter(circuito__municipio__seccion__provincia__eleccion__id=eleccion_id).select_related('circuito')

class ReplicarActasForm(forms.Form):
    municipio_origen = forms.ModelChoiceField(queryset = Municipio.objects.all())
    municipio_destino = forms.ModelChoiceField(queryset = Municipio.objects.all())
    def __init__(self, *args, **kwargs):
        eleccion_id = kwargs.pop('eleccion_id', None)
        super(ReplicarActasForm, self).__init__(*args, **kwargs)
        if eleccion_id:
            self.fields['municipio_origen'].queryset = Municipio.objects.filter(seccion__provincia__eleccion=eleccion_id)
            self.fields['municipio_origen'].initial = self.fields['municipio_origen'].queryset.first()
            self.fields['municipio_destino'].queryset = Municipio.objects.filter(seccion__provincia__eleccion=eleccion_id)

class VotanteForm(ModelForm):
    class Meta:
        model= Votante
        fields= ('num_doc', )
