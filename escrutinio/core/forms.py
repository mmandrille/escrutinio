from django import forms

#Import Personales
from .models import Acta
from .models import Padron, Eleccion, Provincia, Municipio, Cargo, Total, Agrupacion, Lista

class ActaForm(forms.ModelForm):
    mesa = forms.IntegerField(widget=forms.TextInput(attrs={'readonly':'readonly'}))
    celectores = forms.IntegerField(widget=forms.TextInput(attrs={'readonly':'readonly'}))
    class Meta:
        model = Acta
        fields = ('mesa', 'celectores', 'escaneado')

class UploadCsv(forms.Form):
    eleccion = forms.ModelChoiceField(queryset=Eleccion.objects.all())
    provincia = forms.ModelChoiceField(queryset=Provincia.objects.all())
    csvfile = forms.FileField(widget=forms.FileInput(attrs={'accept': ".csv"}))

class UploadPadron(forms.Form):
    eleccion = forms.ModelChoiceField(queryset=Eleccion.objects.all())
    provincia = forms.ModelChoiceField(queryset=Provincia.objects.all())
    fecha = forms.IntegerField()
    descripcion = forms.CharField()
    csvfile = forms.FileField(widget=forms.FileInput(attrs={'accept': ".csv"}))

class UploadMesasTestigo(forms.Form):
    eleccion = forms.ModelChoiceField(queryset=Eleccion.objects.all())
    provincia = forms.ModelChoiceField(queryset=Provincia.objects.all())
    padron = forms.ModelChoiceField(queryset=Padron.objects.all())
    csvfile = forms.FileField(widget=forms.FileInput(attrs={'accept': ".csv"}))

class UploadVotosForm(forms.Form):
    eleccion = forms.ModelChoiceField(queryset=Eleccion.objects.all())
    municipio_destino = forms.ModelChoiceField(queryset = Municipio.objects.all())
    csvfile = forms.FileField(widget=forms.FileInput(attrs={'accept': ".csv"}))
    def __init__(self, *args, **kwargs):
        eleccion_id = kwargs.pop('eleccion_id', None)
        super(UploadVotosForm, self).__init__(*args, **kwargs)
        if eleccion_id:
            self.fields['municipio_destino'].queryset = Municipio.objects.filter(seccion__provincia__eleccion=eleccion_id)

class ReplicarEleccionForm(forms.Form):
    eleccion_vieja = forms.ModelChoiceField(queryset=Eleccion.objects.filter(activa=True))
    provincia_vieja = forms.ModelChoiceField(queryset=Provincia.objects.filter(eleccion__activa=True))
    eleccion_nueva = forms.ModelChoiceField(queryset=Eleccion.objects.filter(activa=False))
    csvgeo = forms.FileField(widget=forms.FileInput(attrs={'accept': ".csv"}))
    csvmesas = forms.FileField(widget=forms.FileInput(attrs={'accept': ".csv"}))
    bool_parametrizar = forms.BooleanField()
    bool_partidos = forms.BooleanField()
    def __init__(self, *args, **kwargs):
        super(ReplicarEleccionForm, self).__init__(*args, **kwargs)
        self.fields['csvgeo'].required = False
        self.fields['csvmesas'].required = False
        self.fields['bool_parametrizar'].initial = True
        self.fields['bool_parametrizar'].required = False
        self.fields['bool_partidos'].initial = True
        self.fields['bool_partidos'].required = False

class DownloadOficialesForm(forms.Form):
    eleccion_destino = forms.ModelChoiceField(queryset=Eleccion.objects.filter(activa=False))
    link = forms.URLField()
    usuario = forms.CharField()
    password = forms.CharField()
    mesa_inicio = forms.IntegerField()
    mesa_fin = forms.IntegerField()
    def __init__(self, *args, **kwargs):
        super(DownloadOficialesForm, self).__init__(*args, **kwargs)
        self.fields['eleccion_destino'].initial = Eleccion.objects.filter(activa=False).first()
        self.fields['link'].initial = 'http://www.eleccionesjujuy2019.com.ar/apwsmesa01.aspx?wsdl'
        self.fields['usuario'].initial = 'MMandrille'
        self.fields['password'].initial = '3537048459'
        self.fields['mesa_inicio'].initial = 1
        self.fields['mesa_fin'].initial = 1

class CruceDiferencial(forms.Form):
    eleccion1 = forms.ModelChoiceField(queryset=Eleccion.objects.filter(activa=True))
    municipio1 = forms.ModelChoiceField(queryset=Municipio.objects.filter(seccion__provincia__eleccion__activa=True).order_by('alpha_id'))
    eleccion2 = forms.ModelChoiceField(queryset=Eleccion.objects.filter(activa=False))
    diff_minima = forms.IntegerField()
    cargos = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple())
    agrupaciones = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple())
    download = forms.BooleanField()
    def __init__(self, *args, **kwargs):
        super(CruceDiferencial, self).__init__(*args, **kwargs)
        self.fields['eleccion1'].initial = Eleccion.objects.filter(activa=True).first()
        self.fields['municipio1'].required = False
        self.fields['eleccion2'].initial = Eleccion.objects.filter(activa=False).first()
        self.fields['diff_minima'].initial = 15
        self.fields['diff_minima'].label = 'Differencia Minima'
        self.fields['cargos'].choices = [[c.orden, c.nombre] for c in Cargo.objects.filter(eleccion__activa=True).order_by('orden')]
        self.fields['agrupaciones'].choices = [[a.alpha_id, str(a.alpha_id)+'-'+a.nombre] for a in Agrupacion.objects.filter(eleccion__activa=True).order_by('alpha_id')]
        self.fields['download'].required = False

class ControlEstadistico(forms.Form):
    eleccion = forms.ModelChoiceField(queryset=Eleccion.objects.filter(activa=True))
    municipio = forms.ModelChoiceField(queryset=Municipio.objects.filter(seccion__provincia__eleccion__activa=True).order_by('alpha_id'))
    diff_minima = forms.IntegerField()
    cargos = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple())
    agrupaciones = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple())
    download = forms.BooleanField()
    def __init__(self, *args, **kwargs):
        super(ControlEstadistico, self).__init__(*args, **kwargs)
        self.fields['eleccion'].initial = Eleccion.objects.filter(activa=True).first()
        self.fields['municipio'].initial = Municipio.objects.filter(seccion__provincia__eleccion__activa=True).first()
        self.fields['diff_minima'].initial = 15
        self.fields['diff_minima'].label = 'Differencia Minima'
        self.fields['cargos'].choices = [[c.orden, c.nombre] for c in Cargo.objects.filter(eleccion__activa=True).order_by('orden')]
        self.fields['agrupaciones'].choices = [[a.alpha_id, str(a.alpha_id)+'-'+a.nombre] for a in Agrupacion.objects.filter(eleccion__activa=True).order_by('alpha_id')]
        self.fields['download'].required = False

class ControlNoPositivos(forms.Form):
    eleccion = forms.ModelChoiceField(queryset=Eleccion.objects.all())
    municipio = forms.ModelChoiceField(queryset=Municipio.objects.filter(seccion__provincia__eleccion__activa=True).order_by('alpha_id'))
    maximo = forms.IntegerField()
    cargos = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple())
    totales = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple())
    download = forms.BooleanField()
    def __init__(self, *args, **kwargs):
        super(ControlNoPositivos, self).__init__(*args, **kwargs)
        self.fields['eleccion'].initial = Eleccion.objects.all().first()
        self.fields['municipio'].required = False
        self.fields['maximo'].initial = 15
        self.fields['maximo'].label = 'Votos Maximos'
        self.fields['cargos'].choices = [[c.orden, c.nombre] for c in Cargo.objects.filter(eleccion__activa=True).order_by('orden')]
        self.fields['totales'].choices = [[t.orden, t.nombre] for t in Total.objects.filter(eleccion__activa=True, totales=False).order_by('orden')]
        self.fields['download'].required = False

class ControlxLimites(forms.Form):
    eleccion = forms.ModelChoiceField(queryset=Eleccion.objects.all())
    municipio = forms.ModelChoiceField(queryset=Municipio.objects.filter(seccion__provincia__eleccion__activa=True).order_by('alpha_id'))
    cargos = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple())
    minimo = forms.IntegerField()
    maximo = forms.IntegerField()
    agrupaciones = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple())
    download = forms.BooleanField()
    def __init__(self, *args, **kwargs):
        super(ControlxLimites, self).__init__(*args, **kwargs)
        self.fields['eleccion'].initial = Eleccion.objects.all().first()
        self.fields['municipio'].required = False
        self.fields['cargos'].choices = [[c.orden, c.nombre] for c in Cargo.objects.filter(eleccion__activa=True).order_by('orden')]
        self.fields['minimo'].initial = 1
        self.fields['minimo'].label = 'Votos Minimos Aceptables'
        self.fields['maximo'].initial = 200
        self.fields['maximo'].label = 'Votos Maximos Aceptables'
        self.fields['agrupaciones'].choices = [[a.alpha_id, str(a.alpha_id)+'-'+a.nombre] for a in Agrupacion.objects.filter(eleccion__activa=True).order_by('alpha_id')]
        self.fields['agrupaciones'].initial = Agrupacion.objects.filter(eleccion__activa=True).first()
        self.fields['download'].required = False