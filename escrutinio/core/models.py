from __future__ import unicode_literals
import datetime
from django.db import models
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.forms.widgets import CheckboxSelectMultiple
from django.core.exceptions import ObjectDoesNotExist
from django.core.cache import cache
from django.utils.timezone import now
#Modulos extras:
from tinymce.models import HTMLField
#Agregar Modulos personales

#Como hacer backup:
#python3 manage.py dumpdata > datadump.json
#python3 manage.py loaddata datadump.json

#Choices Definitos
TIPO_CARGO=(
        (1, 'Nacional'),
        (2, 'Provincial'),
        (3, 'Municipal'),
)

TIPO_PODER=(
        (1, 'Ejecutivo'),
        (2, 'Legislativo'),
)

TIPO_COLUMNA=(
        (1, 'Carga'),
        (2, 'Resultado'),
)

TIPO_RESPONSABLE = (
        (1, 'Responsable de Municipio'),
        (2, 'Responsable de Circuito'),
        (3, 'Responsable de Escuela'),
        (0, 'Fiscal de Mesa'),
)

GENERO=(
        ('M', 'Masculino'),
        ('F', 'Femenino'),
)

#Definimos nuestros modelos aca
#Politica
class Eleccion(models.Model):
    nombre = models.CharField('Nombre', max_length=200)
    fecha = models.DateTimeField('Fecha de Eleccion')
    activa = models.BooleanField('Activa', default=False)
    class Meta:
        verbose_name_plural = 'Elecciones'
    def __str__(self):
        return(self.nombre)

#Parametrizacion
class Columna(models.Model):
    eleccion = models.ForeignKey(Eleccion, on_delete=models.CASCADE, related_name="columnas")
    orden = models.IntegerField(default=0)
    nombre = models.CharField('Nombre', max_length=25)
    tipo = models.IntegerField(choices=TIPO_COLUMNA, default=1)
    show_field = models.IntegerField(choices=(), default=0)
    def __str__(self):
        return(self.nombre)
    def get_cname(self):
        return self._meta.model._meta.model_name

class Cargo(models.Model):
    eleccion = models.ForeignKey(Eleccion, on_delete=models.CASCADE, related_name="cargos")
    orden = models.IntegerField(default=0)
    nombre = models.CharField('Nombre', max_length=200)
    tipo = models.IntegerField(choices=TIPO_CARGO, default=1)
    poder= models.IntegerField(choices=TIPO_PODER, default=1)
    def __str__(self):
        return(self.nombre)
    def get_cname(self):
        return self._meta.model._meta.model_name

class Total(models.Model):
    eleccion = models.ForeignKey(Eleccion, on_delete=models.CASCADE, related_name="totales")
    orden = models.IntegerField(default=0)
    nombre = models.CharField('Nombre', max_length=30)
    totales = models.BooleanField('Totales', default=False)
    class Meta:
        verbose_name = 'Total'
        verbose_name_plural = 'Totales'
    def __str__(self):
        return(self.nombre)

#Geolocalizacion
class Provincia(models.Model):
    eleccion = models.ForeignKey(Eleccion, on_delete=models.CASCADE, related_name="provincias")
    nombre = models.CharField('Nombre', max_length=200)
    def __str__(self):
        return(self.nombre + '(' + self.eleccion.nombre + ') ')

class Seccion(models.Model):#Departamento
    provincia = models.ForeignKey(Provincia, on_delete=models.CASCADE, related_name="secciones")
    alpha_id = models.IntegerField(default=0)
    nombre = models.CharField('Nombre', max_length=200)
    class Meta:
        verbose_name_plural = 'Secciones'
    def __str__(self):
        return(str(self.alpha_id) + ': ' + self.nombre)
    

class Municipio(models.Model):
    seccion = models.ForeignKey(Seccion, on_delete=models.CASCADE, related_name="municipios")
    alpha_id = models.IntegerField(default=0)
    nombre = models.CharField('Nombre', max_length=200)
    def __str__(self):
        return(str(self.alpha_id) + ': ' + self.nombre)

class CantCargos(models.Model):
    cargo = models.ForeignKey(Cargo, on_delete=models.CASCADE, related_name="cantcargos", null=True, blank=True)
    provincia = models.ForeignKey(Provincia, on_delete=models.CASCADE, related_name="cantcargos")
    municipio = models.ForeignKey(Municipio, on_delete=models.CASCADE, related_name="cantcargos", null=True, blank=True)
    cant_escanios= models.IntegerField(default=1)
    def __str__(self):
        if self.provincia:
            return(self.provincia.nombre + ' para ' + self.cargo.nombre + ': ' + str(self.cant_escanios))
        elif self.municipio:
            return(self.municipio.nombre + ' para ' + self.cargo.nombre + ': ' + str(self.cant_escanios))
        else:
            return('Generico para ' + self.cargo.nombre + ': ' + str(self.cant_escanios))

class Circuito(models.Model):
    municipio  = models.ForeignKey(Municipio, on_delete=models.CASCADE, related_name="circuitos")
    alpha_id = models.CharField('ID', max_length=6)
    nombre = models.CharField('Nombre', max_length=200)
    def __str__(self):
        return(str(self.alpha_id) + ': ' + self.nombre)

class Escuela(models.Model):
    circuito = models.ForeignKey(Circuito, on_delete=models.CASCADE, related_name="escuelas")
    numero = models.IntegerField(default=0)
    nombre = models.CharField('Nombre', max_length=200)
    direccion = models.CharField('Direccion', max_length=200, null=True, blank=True)
    def __str__(self):
        return(self.circuito.nombre + '>' + self.nombre)#Siempre que se vaya a displayar traer circuito related

class Mesa(models.Model):
    escuela = models.ForeignKey(Escuela, on_delete=models.CASCADE, related_name="mesas")
    numero = models.IntegerField(default=0)
    extranjeros = models.BooleanField('Mesa de Extranjeros(Solo Municipal)', default=False)
    testigo = models.BooleanField('Mesa Testigo', default=False)
    puntaje = models.FloatField(default=0, null=True, blank=True)
    def __str__(self):
        return('Mesa:' + str(self.numero) + ' (' + self.escuela.nombre + ')')#Siempre que se vaya a displayar traer escuela related

#Partidos
class Agrupacion(models.Model):#Partido Politica
    eleccion = models.ForeignKey(Eleccion, on_delete=models.CASCADE, related_name="agrupaciones")
    alpha_id = models.IntegerField('numero', default=0)
    nombre = models.CharField('Nombre', max_length=200)
    logo = models.FileField('Logo', upload_to='agrupaciones/', null=True, blank=True)
    class Meta:
        verbose_name = 'Agrupacion Politico'
        verbose_name_plural = 'Agrupaciones Politicos'
    def __str__(self):
        return(str(self.alpha_id) + ' ' + self.nombre)

class Lista(models.Model):
    agrupacion = models.ForeignKey(Agrupacion, on_delete=models.CASCADE, related_name="listas")
    orden = models.IntegerField(default=0)
    municipio = models.ForeignKey(Municipio, on_delete=models.CASCADE, related_name="listas")
    nombre = models.CharField('Nombre', max_length=200)
    cargos = models.ManyToManyField(Cargo, blank=True)
    formfield_overrides = {models.ManyToManyField: {'widget': CheckboxSelectMultiple},}
    def __str__(self):
        return(str(self.agrupacion.alpha_id) + ' ' + self.agrupacion.nombre + ': ' + self.nombre + "(" + self.municipio.nombre + ")")

#Votos y Conteo
class Acta(models.Model):
    mesa = models.OneToOneField(Mesa, on_delete=models.CASCADE, related_name="acta")
    escaneado = models.FileField('Acta Escaneada', upload_to='actas/', null=True, blank=True)
    celectores = models.IntegerField(default=0)
    #Podriamos rastrear quien carga las actas!!!
    def __str__(self):
        return("Mesa: " + str(self.mesa.numero) + ", Escuela: " + self.mesa.escuela.nombre)

class Voto(models.Model):
    mesa = models.ForeignKey(Mesa, on_delete=models.CASCADE, related_name="votos")
    lista = models.ForeignKey(Lista, on_delete=models.CASCADE, related_name="votos")
    cargo = models.ForeignKey(Cargo, on_delete=models.CASCADE, related_name="votos")
    cantidad = models.IntegerField(default=0)
    class Meta:
        unique_together = (("mesa", "lista", "cargo"),)
    def __str__(self):
        return('Mesa: ' + str(self.mesa.numero) + " Lista: " + self.lista.nombre + " Para: " + self.cargo.nombre + '=' +str(self.cantidad))

class Voto_Resumen(models.Model):
    municipio = models.ForeignKey(Municipio, on_delete=models.CASCADE, related_name="votos_resumen")
    pescrutado = models.FloatField(null=True, blank=True)#Si es diferente al actual se deben actualizar
    lista = models.ForeignKey(Lista, on_delete=models.CASCADE, related_name="votos_resumen")
    cargo = models.ForeignKey(Cargo, on_delete=models.CASCADE, related_name="votos_resumen")
    cantidad = models.IntegerField(default=0)
    class Meta:
        unique_together = (("municipio", "lista", "cargo"),)
    def __str__(self):
        return('Municipio: ' + str(self.municipio.nombre) + " Lista: " + self.lista.nombre + " Para: " + self.cargo.nombre + '=' +str(self.cantidad))

class Conteo(models.Model):
    mesa = models.ForeignKey(Mesa, on_delete=models.CASCADE, related_name="conteos")
    total = models.ForeignKey(Total, on_delete=models.CASCADE, related_name="conteos")
    cargo = models.ForeignKey(Cargo, on_delete=models.CASCADE, related_name="conteos")
    cantidad = models.IntegerField(default=0)
    rellenado = models.BooleanField('Rellenado por Sistema', default=False)
    class Meta:
        unique_together = (("mesa", "total", "cargo"),)
    def __str__(self):
        return('Mesa:' + str(self.mesa.numero) + " Concepto:" + self.total.nombre + " Para:" + self.cargo.nombre + '=' +str(self.cantidad))

class Conteo_Resumen(models.Model):
    municipio = models.ForeignKey(Municipio, on_delete=models.CASCADE, related_name="conteos_resumen")
    pescrutado = models.FloatField(null=True, blank=True)#Si es diferente al actual se deben actualizar
    total = models.ForeignKey(Total, on_delete=models.CASCADE, related_name="conteos_resumen")
    cargo = models.ForeignKey(Cargo, on_delete=models.CASCADE, related_name="conteos_resumen")
    cantidad = models.IntegerField(default=0)
    class Meta:
        unique_together = (("municipio", "total", "cargo"),)
    def __str__(self):
        return('Municipio: ' + str(self.municipio.nombre) + " Concepto:" + self.total.nombre + " Para:" + self.cargo.nombre + '=' +str(self.cantidad))

class Denuncia(models.Model):
    municipio = models.ForeignKey(Municipio, on_delete=models.CASCADE, related_name="denuncias")
    escuela = models.ForeignKey(Escuela, on_delete=models.CASCADE, related_name="denuncias")
    mesa = models.ForeignKey(Mesa, on_delete=models.CASCADE, related_name="denuncias", null=True, blank=True)
    texto = HTMLField()
    def __str__(self):
        return("Localidad:" + self.municipio.nombre + ": " + self.texto[:50])

class Responsable(models.Model):
    eleccion = models.ForeignKey(Eleccion, on_delete=models.CASCADE, related_name="responsables")
    tipo = models.IntegerField(choices=TIPO_RESPONSABLE, default=0)
    municipio = models.ForeignKey(Municipio, on_delete=models.CASCADE, related_name="responsables", null=True, blank=True)
    circuito = models.ForeignKey(Circuito, on_delete=models.CASCADE, related_name="responsables", null=True, blank=True)
    escuela = models.ForeignKey(Escuela, on_delete=models.CASCADE, related_name="responsables", null=True, blank=True)
    mesa = models.ForeignKey(Mesa, on_delete=models.CASCADE, related_name="responsables", null=True, blank=True)
    nombres = models.CharField('Nombres', max_length=50)
    apellido = models.CharField('Apellidos', max_length=50)
    foto = models.FileField('Foto de Perfil', upload_to='foto_responsable/', null=True, blank=True)
    num_doc = models.CharField('Numero de Documento', max_length=50, null=True, blank=True)
    telefono = models.CharField('Telefono', max_length=20, null=True, blank=True)
    email = models.EmailField('Correo Electronico Personal', null=True, blank=True)
    autorizado = models.BooleanField('Autorizado', default=True)
    def __str__(self):
        return(self.nombres + ' ' + self.apellido + " (" + self.get_tipo_display() + ")")

#sistema de manejo de padrones:
class Padron(models.Model):
    provincia = models.ForeignKey(Provincia, on_delete=models.CASCADE, related_name="padrones")
    descripcion = HTMLField(null=True, blank=True)
    fecha = models.IntegerField('Año', default=0)
    actual = models.BooleanField('Actual', default=True)
    def __str__(self):
        return(self.provincia.nombre + ' ' + str(self.fecha))
    class Meta:
        unique_together = (("provincia", "fecha"),)
        verbose_name = 'Padron'
        verbose_name_plural = 'Padrones'

class Votante(models.Model):
    padron = models.ForeignKey(Padron, on_delete=models.CASCADE, related_name="votantes")
    mesa = models.ForeignKey(Mesa, on_delete=models.CASCADE, related_name="votantes")
    tipo_doc = models.CharField('Tipo Documento', max_length=10)
    num_doc = models.IntegerField('Num de Documento')
    apellido = models.CharField('Apellidos', max_length=50)
    nombre = models.CharField('Nombres', max_length=50)
    clase = models.IntegerField('Año de Nacimiento')
    genero = models.CharField(choices=GENERO, max_length=1)
    domicilio = models.CharField('Nombres', max_length=100)
    def __str__(self):
        return(str(self.num_doc) + ': ' + self.apellido + ',' + self.nombre)

#Sistema de Backgrounds
class Progress_Links(models.Model):
    tarea = models.CharField('Tarea', max_length=50)
    inicio = models.DateTimeField(default=now)
    progress_url = models.URLField('Web', blank=True, null=True)